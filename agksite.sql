-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 08, 2018 at 12:27 AM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.5-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agksite`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan_id` int(2) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `url_foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `nama`, `jabatan_id`, `alamat`, `no_telp`, `url_foto`) VALUES
(11, 'Dimas Anderson', 1, 'Gebang Kidul', '085736212108', 'dimas-anderson.png'),
(12, 'Irena Musdhalifah Perdani', 2, 'Gebang Kidul', '085736212009', 'IRENAAA.jpg'),
(13, 'Angela Marsha', 3, 'Gebang Kidul', '085736212010', 'MARSHAAAAAAA.jpg'),
(14, 'Maharani Dwi Endhar P', 4, 'Gebang Kidul', '085736212011', 'raniii.jpg'),
(15, 'Aida Vianisa', 5, 'Gebang Kidul', '085736212012', 'aidaaa.jpg'),
(16, 'Muslim Aswaja', 5, 'Gebang Kidul', '085736212013', 'kangmuss.jpg'),
(17, 'Tamara Denisanary', 5, 'Gebang Rodah 12', '08573621200', 'tamara-denisanary.png'),
(18, 'Kintan Wong', 5, 'Gebang Rodah 13', '08573621233', 'kintan-wong.png'),
(19, 'Frika Pniylachek', 5, 'Gebang Rodah 14', '08573621292', 'friska-pniylachek.png'),
(20, 'William Sudarsono', 5, 'Gebang Rodah 15', '08573621290', 'william-sudarsono.png'),
(21, 'Thomas Wiriwari', 5, 'Gebang Rodah 16', '08573621294', 'thomas-wiriwari.png');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL,
  `jabatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `jabatan`) VALUES
(1, 'Ketua'),
(2, 'Wakil Ketua'),
(3, 'Sekretaris'),
(4, 'Bendahara'),
(5, 'Anggota');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_kegiatan`
--

CREATE TABLE `jadwal_kegiatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `tgl_waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alamat` text NOT NULL,
  `koordinat` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_kegiatan`
--

INSERT INTO `jadwal_kegiatan` (`id`, `nama`, `tgl_waktu`, `alamat`, `koordinat`, `keterangan`) VALUES
(35, 'Akhirus Sannah Tahun Pelajaran 2017/2018', '2018-07-19 01:00:00', 'TK Cahaya Tazkia, Jalan Gebang Putih, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.288064900000002 112.78493400000002', 'Kolaborasi dengan TK Muslimat NU.'),
(36, 'Kampanye Khofifah-Emil', '2018-07-20 02:00:00', 'Kelurahan Gebang Putih, Jl. Gebang Putih Surabaya, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.2831277 112.78559310000003', 'Bersama ibu-ibu Muslimat NU.'),
(37, 'Festival Al Banjari Se-Surabaya', '2018-05-27 02:00:00', 'Masjid Baitur Rouf, Jalan Gebang Wetan, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.282417199999998 112.78898649999996', 'Kolaborasi dengan pengurus masjid Baitur Rouf.'),
(38, 'Sholawat Bersama Habib Syech bin Abdul Qodir As Seggaf', '2018-05-19 12:00:00', 'Warkop Sholawat, Jalan Gebang Kidul, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.2827989 112.78720900000008', 'Kolaborasi dengan Warkop Sholawat.'),
(39, 'Konser Nella Kharisma', '2018-06-03 02:00:00', 'South Gebang Regency, Blk. D Jalan Gebang Wetan, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.283327799999999 112.78652740000007', 'Dalam rangka memeriahkan sunat massal.'),
(40, 'Konser Martin Garrix', '2018-08-10 13:00:00', 'Warung Taman Baca Bucek Juantara, Jalan Gebang Putih, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.283287000000001 112.78553899999997', 'Acara dimulai ba\'da Isya.'),
(41, 'Seminar Tolak Peluru Nasional', '2018-06-09 01:00:00', 'Warung Nasi Uduk, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.283389799999999 112.78820700000006', 'Kolaborasi dengan ikatan tolak peluru se-Nusantara.'),
(42, 'Kompetisi Mobile Legend Se-Gebang Raya', '2018-06-03 01:00:00', 'Rava Home, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.283322199999999 112.78920649999998', 'Acara diawali dengan sholat Dhuha.'),
(43, 'Kaiba Grand Prix', '2018-05-31 13:00:00', 'Warkop TBC, Jalan Gebang Kidul, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.282666 112.78712900000005', 'Kolaborasi dengan Kaiba Corporation.'),
(44, 'Nonton Bareng Deadpool', '2018-07-13 14:00:00', 'ANZ Warkop, Jalan Gebang Kidul, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.2825425 112.78648709999993', 'Acara dimulai ba\'da sholat tarowih.'),
(45, 'Workshop Printing', '2018-06-01 01:00:00', 'Theater Print, Jalan Gebang Lor, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.280340999999998 112.78912700000001', 'Kolaborasi dengan Theater Print.'),
(46, 'Pengajian K.H. Imam Haromain', '2018-06-09 14:00:00', 'Pondok Pesantren Muhyiddin, Jalan Gebang Kidul, Gebang Putih, Surabaya City, East Java, Indonesia', '-7.282861 112.78827690000003', 'Menyambut nuzulul Qur`an.');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_jawaban`
--

CREATE TABLE `jenis_jawaban` (
  `id` int(11) NOT NULL,
  `jenis_jawaban` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_jawaban`
--

INSERT INTO `jenis_jawaban` (`id`, `jenis_jawaban`) VALUES
(1, 'Multiple Choice'),
(2, 'Jawaban Singkat'),
(3, 'Checkboxes');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `telepon` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`telepon`, `email`, `alamat`) VALUES
('085736212022', 'gebangkidul@yahoo.com', 'Jl. Gebang Kidul 01, Surabaya, Jawa Timur');

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner`
--

CREATE TABLE `kuisioner` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner`
--

INSERT INTO `kuisioner` (`id`, `judul`, `aktif`) VALUES
(1, 'Kuisioner Update', 0),
(2, 'Kuisioner Lomba', 1),
(3, 'Kuisioner Acara', 0),
(4, 'Kuisioner Pilkada', 0),
(5, 'Kuisioner Buka Bersama', 0),
(6, 'Coba 2', 0),
(7, 'Coba 3', 0),
(8, 'Coba 4', 0),
(10, 'Kuisioner Hoe', 0),
(11, 'Kuisioner Hoead', 0),
(12, 'Kuisioner Hoeaddede', 0),
(13, 'Kuisioner Defee', 0),
(14, 'Kuisioner Feoinjof', 0),
(15, 'Kuisioner Vfjonfore', 0),
(16, 'Kuisioner Youuu', 0),
(17, 'Kuisioner Hahaha', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pilihan_jawaban`
--

CREATE TABLE `pilihan_jawaban` (
  `id` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `jawaban` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pilihan_jawaban`
--

INSERT INTO `pilihan_jawaban` (`id`, `id_soal`, `jawaban`) VALUES
(3, 3, 'Mobile Legends'),
(4, 3, 'Yu-Gi-Oh! Duel Generation'),
(5, 4, 'Sepak Bola'),
(6, 4, 'Angkat Besi'),
(28, 34, 'Jujur dan bersih'),
(29, 34, 'Tegas'),
(30, 34, 'Berpihak kepada rakyat'),
(31, 34, 'Adil dan bijaksana'),
(32, 34, 'Moderat'),
(33, 34, 'Berpengalaman memimpin'),
(34, 34, 'Berpendidikan'),
(35, 35, 'Ya'),
(36, 35, 'Tidak'),
(37, 35, 'Belum memutuskan'),
(38, 37, 'Kawan Semua'),
(56, 45, 'Yaa Azhiim'),
(57, 45, 'Huwallahu Ahad'),
(58, 45, 'A\'udzu bi Robbil Falaq'),
(59, 45, 'A\'udzu bi Robbin Naas'),
(60, 45, 'Yaa Ayyuhal Kaafiruun');

-- --------------------------------------------------------

--
-- Table structure for table `respon`
--

CREATE TABLE `respon` (
  `id` int(11) NOT NULL,
  `id_transaksi_respon` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `respon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respon`
--

INSERT INTO `respon` (`id`, `id_transaksi_respon`, `id_soal`, `respon`) VALUES
(1, 2, 4, 'Wkwkwk'),
(2, 2, 3, 'Hehehe'),
(3, 1, 3, 'Huauau'),
(4, 4, 3, 'ok'),
(5, 5, 3, 'ok'),
(6, 5, 3, 'ko'),
(7, 6, 34, 'Jujur dan bersih'),
(8, 6, 34, 'Berpihak kepada rakyat'),
(9, 6, 34, 'Moderat'),
(10, 6, 35, 'Ya'),
(11, 6, 36, 'Muslim Aswaja'),
(12, 7, 34, 'Jujur dan bersih'),
(13, 7, 34, 'Adil dan bijaksana'),
(14, 7, 35, 'Tidak'),
(15, 7, 36, 'Barid Lim'),
(16, 8, 34, 'Berpihak kepada rakyat'),
(17, 8, 35, 'Ya'),
(18, 8, 36, 'Ada Dong'),
(19, 9, 3, 'Mobile Legends'),
(20, 9, 4, 'Sepak Bola'),
(21, 9, 5, 'Emboo'),
(22, 10, 3, 'Mobile Legends'),
(23, 10, 4, 'Sepak Bola'),
(24, 10, 5, 'Coba'),
(25, 11, 34, 'Jujur dan bersih'),
(26, 11, 34, 'Tegas'),
(27, 11, 35, 'Ya'),
(28, 11, 36, 'Danang'),
(29, 12, 34, 'Berpihak kepada rakyat'),
(30, 12, 34, 'Adil dan bijaksana'),
(31, 12, 35, 'Tidak'),
(32, 12, 36, 'Dendi'),
(33, 13, 34, 'Jujur dan bersih'),
(34, 13, 34, 'Tegas'),
(35, 13, 35, 'Ya'),
(36, 13, 36, 'Damar'),
(37, 14, 34, 'Berpihak kepada rakyat'),
(38, 14, 34, 'Adil dan bijaksana'),
(39, 14, 34, 'Berpendidikan'),
(40, 14, 35, 'Belum memutuskan'),
(41, 14, 36, 'Bambang'),
(42, 15, 3, 'Mobile Legends'),
(43, 15, 4, 'Angkat Besi'),
(44, 15, 5, 'Kim Younghyun'),
(45, 16, 3, 'Mobile Legends'),
(46, 16, 4, 'Angkat Besi'),
(47, 16, 5, 'Hehe'),
(48, 17, 3, 'Yu-Gi-Oh! Duel Generation'),
(49, 17, 4, 'Angkat Besi'),
(50, 17, 5, 'Absen 2 Kali'),
(51, 18, 3, 'Yu-Gi-Oh! Duel Generation'),
(52, 18, 4, 'Angkat Besi'),
(53, 18, 5, 'Briaaaaannnn'),
(54, 19, 3, 'Mobile Legends'),
(55, 19, 4, 'Sepak Bola'),
(56, 19, 5, 'muslim');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id` int(11) NOT NULL,
  `id_kuisioner` int(11) NOT NULL,
  `soal` varchar(300) NOT NULL,
  `id_jenis_jawaban` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id`, `id_kuisioner`, `soal`, `id_jenis_jawaban`) VALUES
(3, 2, 'Lomba genre digital apa yang Anda inginkan?', 1),
(4, 2, 'Lomba genre olahraga apa yang Anda inginkan?', 3),
(5, 2, 'Siapa nama pacar saya?', 2),
(34, 4, 'Bagaimana Karakter Pemimpin yang Paling Anda Harapkan?', 3),
(35, 4, 'Apakah Anda Berminat Menggunakan Hak Pilih Anda pada Pilkada Mendatang?', 1),
(36, 4, 'Siapakah Nama Anda?', 2),
(37, 17, 'Hello', 1),
(38, 1, 'Siapakah nama pembantu Anda?', 2),
(39, 1, 'Siapakah nama pembantu Anda?', 2),
(45, 10, 'Qul', 1),
(47, 5, 'Berapa budget Anda?', 2);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_respon`
--

CREATE TABLE `transaksi_respon` (
  `id` int(11) NOT NULL,
  `id_kuisioner` int(11) NOT NULL,
  `waktu_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_respon`
--

INSERT INTO `transaksi_respon` (`id`, `id_kuisioner`, `waktu_input`) VALUES
(1, 3, '2018-05-05 06:22:22'),
(2, 2, '2018-05-05 06:22:22'),
(3, 1, '2018-05-05 07:19:33'),
(4, 1, '2018-05-05 07:20:05'),
(5, 1, '2018-05-05 12:35:34'),
(6, 4, '2018-05-05 12:39:53'),
(7, 4, '2018-05-06 16:42:05'),
(8, 4, '2018-05-07 02:56:49'),
(9, 2, '2018-05-07 04:41:10'),
(10, 2, '2018-05-07 04:50:36'),
(11, 4, '2018-05-21 19:05:58'),
(12, 4, '2018-05-21 19:07:23'),
(13, 4, '2018-05-21 19:07:46'),
(14, 4, '2018-05-25 06:47:11'),
(15, 2, '2018-06-04 02:59:35'),
(16, 2, '2018-06-04 03:00:01'),
(17, 2, '2018-06-04 03:02:45'),
(18, 2, '2018-06-04 03:03:01'),
(19, 2, '2018-06-25 03:11:20');

-- --------------------------------------------------------

--
-- Table structure for table `url_gambar`
--

CREATE TABLE `url_gambar` (
  `id` int(4) NOT NULL,
  `url` varchar(200) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `url_gambar`
--

INSERT INTO `url_gambar` (`id`, `url`, `keterangan`) VALUES
(28, 'pexels-photo-433452.jpeg', '17 Agustusan'),
(29, 'pexels-photo-864990.jpeg', 'Senam Ibu PKK'),
(30, 'sailors-all-hands-navy-military.jpg', 'Apel Kerja Bakti'),
(31, '1526060508060.jpg', 'Ngaji'),
(32, 'Screenshot from 2018-03-22 19-05-18.png', 'Angger Upload'),
(35, 'easter-nest-2168504_1920.jpg', 'Cobbbaaa'),
(37, 'Screenshot from 2018-03-22 19-05-181.png', 'Coba'),
(38, 'Screenshot from 2018-03-27 01-26-181.png', 'Coba-coba'),
(39, 'Screenshot from 2018-03-16 02-46-24.png', 'Coba-coba-coba'),
(42, 'IMG-20171010-WA0020.jpg', 'Aqua'),
(43, '6-pop-up-get-key.png', 'Cobi'),
(44, 'qudrot.png', 'Baru Edit Hehe'),
(45, 'pexels-photo.jpg', 'Nyar Maneh'),
(46, 'light_sky_stars_background_85555_1280x800.jpg', 'Hohohihe');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(72) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_anggota`, `username`, `password`) VALUES
(7, 11, '085736212008', '$2y$10$sM2cOC8KwNmx.El06rAfTOLFIzkaS8KGdx/MW8Rh0HtSL9CodKDzK'),
(8, 12, '085736212009', '$2y$10$mq/wpv0.yEZjih2LEylYlODYSOf6ojyFdErRxgw1Rh9V2KlfKVQA6'),
(9, 13, '085736212010', '$2y$10$f4aAxZqoFX85az.kTF1On.iQM5KY08tbPcgVTskZCoiHveVnaM6hS'),
(10, 14, '085736212011', '$2y$10$cljiiW9eBojt55BQTvcZi.wuPw7O6.mjl0.TmfizEVmSh7SBpN4CS'),
(11, 15, '085736212012', '$2y$10$4xeYnetQtjT5A.tWJko4IOzFXuZjMy3/ZpdrM7c1VPRnjH2jk5Tae'),
(12, 16, '085736212013', '$2y$10$taWeXJQxO.imMkfuvq2Bs.Q7UpyfwIDCVF73vAY/ce2VJOi5jY6P6'),
(13, 17, '08573621200', '$2y$10$Jg1AiRvgWxSvkXmF92vI9Oe.M/xhVkzkHKaLzn59/JOv6zwDcVCZ6'),
(14, 18, '08573621223', '$2y$10$GoAxV2z1sMoz0qB19HyrZOD5QQolF1P3w0etQVUDGp0HUI6.jVCz.'),
(15, 19, '08573621292', '$2y$10$qyQD2BzRwFzfpcl3v3COguERW1h/SG7BmFZDYNpe/LFK8cI6OiFSm'),
(16, 20, '08573621290', '$2y$10$Wn95x3FlbzxO1SawtqIiWurX3BM9Bah.1gvXn2ni6mfXDjhF3eULi'),
(17, 21, '08573621294', '$2y$10$M25nhNwxJqFPd0GeqLDP8ORaZgxDBPvlS727VkHgctURPgr3162xO');

-- --------------------------------------------------------

--
-- Table structure for table `visi_misi`
--

CREATE TABLE `visi_misi` (
  `id` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `konten` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visi_misi`
--

INSERT INTO `visi_misi` (`id`, `jenis`, `konten`) VALUES
(1, 'visi', 'Nemen nyang riko'),
(2, 'misi', 'Memberdayakan janda dan anak yatim'),
(4, 'misi', 'Mengusahakan wajib belajar 11 tahun'),
(5, 'misi', 'Menyanyikan lagu Indonesia Raya'),
(8, 'misi', 'Membangun institusi yang unggul di bidang penyelesaian pembayaran dan solusi keuangan bagi nasabah bisnis dan perseorangan.'),
(9, 'misi', 'Memahami beragam kebutuhan nasabah dan memberikan layanan finansial yang tepat demi tercapainya kepuasan optimal bagi nasabah.'),
(11, 'misi', 'Bakti kepada Masyarakat, dengan mengutamakan perlindungan dasar dan pelayanan prima sejalan dengan kebutuhan masyarakat.'),
(12, 'misi', 'Bakti kepada Lingkungan, dengan memberdayakan potensi sumber daya bagi keseimbangan dan kelestarian lingkungan.'),
(13, 'misi', 'Bakti kepada Negara, dengan mewujudkan kinerja terbaik sebagai penyelenggara Program Asuransi Sosial dan Asuransi Wajib, serta Badan Usaha Milik Negara.'),
(14, 'misi', 'Dek tresnaku mung siji kowe'),
(31, 'misi', 'Aku lagi pingin mikir kerjo'),
(32, 'misi', 'Wa \'alaa alihii wa shohbihi wa sallim\'.'),
(33, 'misi', 'Siap ke TKP 86'),
(34, 'misi', 'Sak temene'),
(35, 'misi', 'Welas ring ati'),
(36, 'misi', 'Wes sing ono liyo'),
(37, 'misi', 'Sulung tah sulung'),
(38, 'misi', 'Ngelem enak'),
(39, 'misi', 'Berasa di surga'),
(48, 'misi', 'Young dumb and broke'),
(49, 'misi', 'Ojo disawang ae ta'),
(56, 'misi', 'Sing tenang ben iso mikir');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jabatan_id` (`jabatan_id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_kegiatan`
--
ALTER TABLE `jadwal_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_jawaban`
--
ALTER TABLE `jenis_jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kuisioner`
--
ALTER TABLE `kuisioner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pilihan_jawaban`
--
ALTER TABLE `pilihan_jawaban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pilihanJawabanToSoal` (`id_soal`);

--
-- Indexes for table `respon`
--
ALTER TABLE `respon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaksi_respon` (`id_transaksi_respon`),
  ADD KEY `id_soal` (`id_soal`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `soalToKuisioner` (`id_kuisioner`),
  ADD KEY `soalToJenisJawaban` (`id_jenis_jawaban`);

--
-- Indexes for table `transaksi_respon`
--
ALTER TABLE `transaksi_respon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kuisioner` (`id_kuisioner`);

--
-- Indexes for table `url_gambar`
--
ALTER TABLE `url_gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indexes for table `visi_misi`
--
ALTER TABLE `visi_misi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jadwal_kegiatan`
--
ALTER TABLE `jadwal_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `jenis_jawaban`
--
ALTER TABLE `jenis_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kuisioner`
--
ALTER TABLE `kuisioner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `pilihan_jawaban`
--
ALTER TABLE `pilihan_jawaban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `respon`
--
ALTER TABLE `respon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `transaksi_respon`
--
ALTER TABLE `transaksi_respon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `url_gambar`
--
ALTER TABLE `url_gambar`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `visi_misi`
--
ALTER TABLE `visi_misi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggota`
--
ALTER TABLE `anggota`
  ADD CONSTRAINT `FK_JABATAN_ID` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`id`);

--
-- Constraints for table `pilihan_jawaban`
--
ALTER TABLE `pilihan_jawaban`
  ADD CONSTRAINT `pilihanJawabanToSoal` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `respon`
--
ALTER TABLE `respon`
  ADD CONSTRAINT `fk_soal` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transkasi_respon` FOREIGN KEY (`id_transaksi_respon`) REFERENCES `transaksi_respon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `soalToJenisJawaban` FOREIGN KEY (`id_jenis_jawaban`) REFERENCES `jenis_jawaban` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soalToKuisioner` FOREIGN KEY (`id_kuisioner`) REFERENCES `kuisioner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_respon`
--
ALTER TABLE `transaksi_respon`
  ADD CONSTRAINT `fk_kuisioner` FOREIGN KEY (`id_kuisioner`) REFERENCES `kuisioner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_to_anggota` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
