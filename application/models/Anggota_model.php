<?php

class Anggota_model extends CI_Model {

	private $table = "anggota";

	public function get_all($limit, $offset)
	{
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get_total_halaman()
	{
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		
		$query = $this->db->get($this->table);
		
		return $query->row();
	}

	public function get_for_home()
	{
		$this->db->join('jabatan', 'jabatan.id = anggota.jabatan_id');
		$this->db->where('jabatan', 'Ketua');
		$this->db->or_where('jabatan', 'Sekretaris');
		$this->db->or_where('jabatan', 'Bendahara');
		$this->db->order_by('jabatan.id', 'ASC');
		$this->db->order_by('nama', 'ASC');
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function add($nama, $jabatan_id, $alamat, $no_telp, $url_foto)
	{
		$data = array(
			'nama' => $nama,
			'jabatan_id' => $jabatan_id,
			'alamat' => $alamat,
			'no_telp' => $no_telp,
			'url_foto' => $url_foto
		);
		
		$this->db->insert($this->table, $data);
		
		return $this->db->insert_id();
	}

	public function set($id, $nama, $jabatan_id, $alamat, $no_telp, $url_foto)
	{
		$data = array(
			'nama' => $nama,
			'jabatan_id' => $jabatan_id,
			'alamat' => $alamat,
			'no_telp' => $no_telp,
			'url_foto' => $url_foto
		);
		
		$this->db->where('id', $id);
		
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		
		return $this->db->delete($this->table);
	}
	
}

?>
