<?php

class Kuisioner_model extends CI_Model {

	private $table = 'kuisioner';

	public function get_all($limit, $offset)
	{
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		
		$query = $this->db->get($this->table);
		
		return $query->row();
	}
	
	public function get_total_halaman()
	{
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
	}

	public function get_active()
	{
		$this->db->where('aktif', 1);
		
		$query = $this->db->get($this->table);
		
		return $query->row();
	}
	
	public function add($judul)
	{
		$data = array(
			'judul' => $judul
		);
		
		$this->db->insert($this->table, $data);
		
		return $this->db->insert_id();
	}
	
	public function set($id, $judul)
	{
		$data = array(
			'judul' => $judul
		);
		
		$this->db->where("id", $id);
		return $this->db->update($this->table, $data);
	}
	
	public function set_active($id)
	{
		$to_return = false;
		$data = array(
			'aktif' => FALSE
		);
		
		if($this->db->update($this->table, $data)) {
			$data = array(
				'aktif' => TRUE
			);
			
			$this->db->where("id", $id);
			$to_return = $this->db->update($this->table, $data);
		}
		
		return $to_return;
	}
	
	public function set_nonactive($id)
	{
		$to_return = false;
		$data = array(
			'aktif' => FALSE
		);
			
		$this->db->where("id", $id);
		$to_return = $this->db->update($this->table, $data);
		
		return $to_return;
	}
	
	public function delete($id)
	{
		$this->db->where('id', $id);
		
		return $this->db->delete($this->table);
	}

}

?>
