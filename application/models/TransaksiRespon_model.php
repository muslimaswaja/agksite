<?php

class TransaksiRespon_model extends CI_Model {

	private $table = 'transaksi_respon';

	public function get_all()
	{
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get($id_kuisioner)
	{
		$this->db->where('id_kuisioner', $id_kuisioner);
		$this->db->order_by('id','DESC');
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function add($id_kuisioner)
	{
		$data = array(
			'id_kuisioner' => $id_kuisioner
		);
		
		$this->db->insert($this->table, $data);
		
		return $this->db->insert_id();
	}

}

?>
