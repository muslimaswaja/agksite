<?php

class Respon_model extends CI_Model {

	private $table = 'respon';

	public function get_all()
	{
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get($id_transaksi_respon)
	{
		$this->db->where('id_transaksi_respon', $id_transaksi_respon);
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function add($id_transaksi_respon, $id_soal, $respon)
	{
		$data = array(
			'id_transaksi_respon' => $id_transaksi_respon,
			'id_soal' => $id_soal,
			'respon' => $respon
		);
		
		return $this->db->insert($this->table, $data);
	}

}

?>
