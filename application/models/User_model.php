<?php

class User_model extends CI_Model {

	private $id;
	private $id_anggota;
	private $username;
	private $password;

	private $table = 'user';

	public function add($id_anggota, $username, $password)
	{
		$data = array(
			'id_anggota' => $id_anggota,
			'username' => $username,
			'password' => $password,
		);
		
		return $this->db->insert($this->table, $data);
	}

	public function login($username, $password)
	{
		$to_return = false;
		
		$this->db->where('username', $username);
		
		$query = $this->db->get($this->table);
		
		if($query->row() != null) {
			$to_return = password_verify($password, $query->row()->password);
		}
		
		return $to_return;
	}

}

?>
