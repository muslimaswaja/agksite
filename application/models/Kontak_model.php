<?php

class Kontak_model extends CI_Model {

	public $telepon;
	public $email;
	public $alamat;
	private $table = 'kontak';

	public function get()
	{
		$query = $this->db->get($this->table);
		
		return $query->row();
	}

	public function set($telepon, $email, $alamat)
	{
		$data = array(
			'telepon' => $telepon,
			'email' => $email,
			'alamat' => $alamat
		);
		
		return $this->db->update($this->table, $data);
	}

}

?>
