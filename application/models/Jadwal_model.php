<?php

class Jadwal_model extends CI_Model {

	private $table = 'jadwal_kegiatan';

	public function get_all($limit, $offset)
	{
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->table);
		
		return $query->result();
	}
	
	public function get_total_halaman()
	{
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
	}

	public function get_for_home()
	{
		$this->db->where('tgl_waktu >= NOW()');
		$this->db->order_by('tgl_waktu', 'ASC');
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		
		$query = $this->db->get($this->table);
		
		return $query->row();
	}
	
	public function add($nama, $tgl_waktu, $alamat, $koordinat, $keterangan)
	{
		$data = array(
			'nama' => $nama,
			'tgl_waktu' => $tgl_waktu,
			'alamat' => $alamat,
			'koordinat' => $koordinat,
			'keterangan' => $keterangan
		);
		
		$this->db->insert($this->table, $data);
		
		return $this->db->insert_id();
	}
	
	public function set($id, $nama, $tgl_waktu, $alamat, $koordinat, $keterangan)
	{
		$data = array(
			'nama' => $nama,
			'tgl_waktu' => $tgl_waktu,
			'alamat' => $alamat,
			'koordinat' => $koordinat,
			'keterangan' => $keterangan
		);
		
		$this->db->where('id', $id);
		
		return $this->db->update($this->table, $data);
	}
	
	public function delete($id)
	{
		$this->db->where('id', $id);
		
		return $this->db->delete($this->table);
	}

}

?>
