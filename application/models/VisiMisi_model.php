<?php

class VisiMisi_model extends CI_Model {

	public $id;
	public $jenis;
	public $konten;

	private $table = 'visi_misi';

	public function get_visi()
	{
		$this->db->where('jenis', 'visi');
		
		$query = $this->db->get($this->table);
		
		return $query->row()->konten;
	}
	
	public function get_total_halaman()
	{
		$this->db->where('jenis', 'misi');
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
	}

	public function get_misi($limit, $offset)
	{
		$this->db->where('jenis', 'misi');
		$this->db->limit($limit, $offset);
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get_misi_all()
	{
		$this->db->where('jenis', 'misi');
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		
		$query = $this->db->get($this->table);
		
		return $query->row();
	}

	public function add($konten)
	{
		$data = array(
			'jenis' => 'misi',
			'konten' => $konten
		);
		
		return $this->db->insert($this->table, $data);
	}

	public function set($id, $konten)
	{
		$data = array(
			'konten' => $konten
		);
		
		$this->db->where('id', $id);
		
		return $this->db->update($this->table, $data);
	}
	
	public function delete($id)
	{
		$this->db->where('id', $id);
		
		return $this->db->delete($this->table);
	}

}

?>
