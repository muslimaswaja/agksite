<?php

class Gambar_model extends CI_Model {

	public $id;
	public $url;
	private $table = 'url_gambar';

	public function get_all($limit, $offset)
	{
		$this->db->limit($limit, $offset);
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function get_for_home()
	{
		$query = $this->db->get($this->table);
		
		return $query->result();
	}
	
	public function get_total_halaman()
	{
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		
		return $query->row();
	}

	public function add($url, $keterangan)
	{
		$data = array(
			'url' => $url,
			'keterangan' => $keterangan
		);
		
		return $this->db->insert($this->table, $data);
	}

	public function set($id, $url, $keterangan)
	{
		$this->db->where('id', $id);
		
		$data = array(
			'url' => $url,
			'keterangan' => $keterangan
		);
		
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		
		return $this->db->delete($this->table);
	}

}

?>
