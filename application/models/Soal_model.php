<?php

class Soal_model extends CI_Model {

	private $table = 'soal';

	public function get_all()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function get_from_kuisioner($id)
	{
		$this->db->where('id_kuisioner', $id);
		$query = $this->db->get($this->table);
		
		return $query->result();
	}
	
	public function add($id_kuisioner, $id_jenis_jawaban, $soal)
	{
		$data = array(
			'id_kuisioner' => $id_kuisioner,
			'id_jenis_jawaban' => $id_jenis_jawaban,
			'soal' => $soal
		);
		
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	public function delete_all($id_kuisioner)
	{
		$this->db->where('id_kuisioner', $id_kuisioner);
		
		return $this->db->delete($this->table);
	}

}

?>
