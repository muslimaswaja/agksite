<?php

class jenisJawaban_model extends CI_Model {

	private $table = 'jenis_jawaban';

	public function get_all()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

}

?>
