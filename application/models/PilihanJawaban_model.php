<?php

class PilihanJawaban_model extends CI_Model {

	private $table = 'pilihan_jawaban';

	public function get_all()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get($id)
	{
		$this->db->where('id', $id);
		
		$query = $this->db->get($this->table);
		
		return $query->row();
	}

	public function get_from_soal($id)
	{
		$this->db->where('id_soal', $id);
		
		$query = $this->db->get($this->table);
		
		return $query->result();
	}

	public function add($id_soal, $jawaban)
	{
		$data = array(
			'id_soal' => $id_soal,
			'jawaban' => $jawaban
		);
		
		return $this->db->insert($this->table, $data);
	}
	
	public function delete_all($id_soal)
	{
		$this->db->where('id_soal', $id_soal);
		
		return $this->db->delete($this->table);
	}

}

?>
