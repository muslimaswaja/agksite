<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gambar extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('gambar_model');
		$this->load->helper('url');
	}
	
	public function index()
	{
		$this->load->view("dev/gambar");
	}
	
	public function set()
	{
		$this->load->view("dev/gambar_set");
	}
	
}
