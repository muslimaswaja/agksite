<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('anggota_model');
		$this->load->helper('url');
	}
	
	public function index()
	{
		$this->load->view("dev/anggota");
	}
	
	public function set()
	{
		$this->load->view("dev/anggota_set");
	}
	
}
