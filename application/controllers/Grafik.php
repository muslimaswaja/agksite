<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require_once(__DIR__.'/api/Anggota.php');
/**
* Controller anggota
*/
class Grafik extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		
		if(!$this->session->has_userdata('username')) {
			redirect('login');
		}
	}

	public function admin_index($id)
	{
		$data["id"] = $id;
		
		$this->load->view('admin/grafik_view.php', $data);
	}

}
?>
