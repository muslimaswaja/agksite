<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		
		if(!$this->session->has_userdata('username')) {
			redirect('login');
		}
	}

	public function admin_index()
	{
		$this->load->view('admin/jadwal-kegiatan_view_list');
	}

	public function admin_tambah()
	{
		$this->load->view('admin/jadwal-kegiatan_view_tambah');
	}

	public function admin_simpan()
	{
		$this->session->set_flashdata('status', 'true');
		redirect(base_url('admin/jadwal'));
	}

	public function admin_edit($id)
	{
		$data["id"] = $id;
		
		$this->load->view('admin/jadwal-kegiatan_view_edit', $data);
	}
}
