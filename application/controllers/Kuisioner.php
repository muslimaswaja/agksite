<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuisioner extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		
		$allowed = [base_url('kuisioner')];
		//~ if(!$this->session->has_userdata('username')) {
			//~ if(!(in_array(current_url(), $allowed))){
				//~ redirect('login');
			//~ }
		//~ }
	}

	public function index()
	{
		$this->load->view('template/navbar_guest.php');
		$this->load->view('guest/kuisioner_view');
		$this->load->view('template/footer_guest.php');
	}

	public function sukses()
	{
		$this->load->view('template/navbar_guest.php');
		$this->load->view('guest/kuisioner_respon');
		$this->load->view('template/footer_guest.php');
	}

	public function admin_index()
	{
		$this->load->view('admin/kuisioner_view_list');
	}

	public function admin_edit($id)
	{
		$data["id"] = $id;
		
		$this->load->view('admin/kuisioner_view_edit',$data);
	}
}
