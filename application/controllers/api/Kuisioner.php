<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuisioner extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('kuisioner_model');
		$this->load->model('soal_model');
		$this->load->model('pilihanJawaban_model');
		$this->load->model('jenisJawaban_model');
		
		header('Access-Control-Allow-Origin: *');
	}

	public function get_total_halaman()
	{
		$data = $this->kuisioner_model->get_total_halaman()/10;
		
		if(fmod($data,1) > 0) {
			$data = $data - fmod($data,1) + 1;
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_all($halaman=1)
	{
		$limit = 10;
		$offset = ($halaman-1)*10;
		$data = $this->kuisioner_model->get_all($limit, $offset);
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	
	public function get($id)
	{
		$data = $this->kuisioner_model->get($id);
		
		if(count($this->soal_model->get_from_kuisioner($id)) != 0) {
			$data->soal = $this->soal_model->get_from_kuisioner($id);
			
			foreach($data->soal as $soal) {
				$soal->jenis_jawaban = $this->jenisJawaban_model->get($soal->id_jenis_jawaban);
				
				if($soal->id_jenis_jawaban != 2) {
					$soal->jawaban = $this->pilihanJawaban_model->get_from_soal($soal->id);
				}
			}
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_active()
	{
		$data = $this->kuisioner_model->get_active();
		
		if($data != null) {
			$data->soal = $this->soal_model->get_from_kuisioner($data->id);
			
			foreach($data->soal as $soal) {
				$soal->jenis_jawaban = $this->jenisJawaban_model->get($soal->id_jenis_jawaban);
				
				if($soal->id_jenis_jawaban != 2) {
					$soal->jawaban = $this->pilihanJawaban_model->get_from_soal($soal->id);
				}
			}
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function add()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $id_kuisioner = $this->kuisioner_model->add($data->judul);
		
		if(isset($data->soal) && count($data->soal) > 0) {
			foreach($data->soal as $soal) {
				$id_soal = $this->soal_model->add($id_kuisioner, $soal->id_jenis_jawaban, $soal->soal);
				
				if(isset($soal->jawaban)) {
					foreach($soal->jawaban as $jawaban) {
						$result = $this->pilihanJawaban_model->add($id_soal, $jawaban->jawaban);
					}
				}
			}
		}
		
		echo json_encode($result);
	}
	
	public function set()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$daftarSoal = $this->soal_model->get_from_kuisioner($data->id);
		$to_return = false;
		
		foreach($daftarSoal as $soal) {
			$this->pilihanJawaban_model->delete_all($soal->id);
		}
		
		$this->soal_model->delete_all($data->id);
		
		if($this->kuisioner_model->set($data->id, $data->judul)) {
			$to_return = true;
		}
			
		foreach($data->soal as $soal) {
			$id_soal = $this->soal_model->add($data->id, $soal->id_jenis_jawaban, $soal->soal);
			
			if(isset($soal->jawaban)) {
				foreach($soal->jawaban as $jawaban) {
					$this->pilihanJawaban_model->add($id_soal, $jawaban->jawaban);
				}
			}
		}
		
		echo json_encode($to_return);
	}
	
	public function set_active()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $this->kuisioner_model->set_active($data->id);
		
		echo json_encode($result);
	}
	
	public function set_nonactive()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $this->kuisioner_model->set_nonactive($data->id);
		
		echo json_encode($result);
	}
	
	public function delete($id)
	{
		header('Content-type: application/json');
		
		$result = $this->kuisioner_model->delete($id);
		
		echo json_encode($result);
	}
}
