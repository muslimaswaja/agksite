<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gambar extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('gambar_model');
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function get_all($halaman=1)
	{
		$limit = 10;
		$offset = ($halaman-1)*10;
		$data = $this->gambar_model->get_all($limit, $offset);
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_total_halaman()
	{
		$data = $this->gambar_model->get_total_halaman()/10;
		
		if(fmod($data,1) > 0) {
			$data = $data - fmod($data,1) + 1;
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_for_home()
	{
		$data = $this->gambar_model->get_for_home();
		
		if(count($data) == 0) {
			$image = new \stdClass;
			$image->id = 0;
			$image->url = "default/slideshow.jpg";
			$image->keterangan = "Gambar Default";
			
			array_push($data, $image);
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get($id)
	{
		$data = $this->gambar_model->get($id);
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function add()
	{
		header('Content-Type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $this->gambar_model->add(
			$data->url, $data->keterangan
		);
		
		echo json_encode($result);
	}
	
	public function upload() {
		$to_return = new \stdClass;
		$to_return->status = false;
		$to_return->url = "";
		$target_dir = "assets/images/";
		$nama_asli = pathinfo(
			$_FILES['gambar']['name'], PATHINFO_FILENAME
		);
		$ekstensi = pathinfo(
			$_FILES['gambar']['name'], PATHINFO_EXTENSION
		);
		$increment = 0; 
		$nama_baru = $nama_asli . '.' . $ekstensi;
		
		while(is_file($target_dir . $nama_baru)) {
			$increment++;
			$nama_baru = $nama_asli . $increment . '.' . $ekstensi;
		}
		
		$target_file = $target_dir . $nama_baru;
		
		header('Content-Type: application/json');
		
		if (move_uploaded_file(
			$_FILES["gambar"]["tmp_name"], $target_file
		)) {
			$this->resize($target_file);
			$this->crop($target_file);
			
			$to_return->status = true;
			$to_return->url = $nama_baru;
		}
		
		echo json_encode($to_return);
	}
	
	private function resize($image_url) {
		$config['image_library'] = 'gd2';
		$config['source_image'] = $image_url;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 1640;

		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
	}
	
	private function crop($image_url) {
		$config['image_library'] = 'gd2';
		$config['source_image'] = $image_url;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['width'] = 1640;
		$config['height'] = 480;
		$config['x_axis'] = 0;
		$config['y_axis'] = 0;

		$this->image_lib->initialize($config);

		$this->image_lib->crop();
	}
	
	public function set()
	{
		header('Content-Type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		
		if($data->url == "") {
			$url = $this->gambar_model->get($data->id)->url;
		} else {
			$url = $this->gambar_model->get($data->id)->url;
			
			unlink('assets/images/'.$url);
			
			$url = $data->url;
		}
		
		$result = $this->gambar_model->set(
			$data->id, $url, $data->keterangan
		);
		
		echo json_encode($result);
	}
	
	public function delete($id)
	{
		$url = $this->gambar_model->get($id)->url;
		$to_return = false;
		
		if(unlink('assets/images/'.$url)) {
			if($this->gambar_model->delete($id)) {
				$to_return = true;
			}
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($to_return);
	}
	
}
