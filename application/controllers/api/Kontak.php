<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('kontak_model');
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function get()
	{
		$data = $this->kontak_model->get();
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function set()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $this->kontak_model->set(
			$data->telepon, $data->email, $data->alamat
		);
		
		echo json_encode($result);
	}
	
}
