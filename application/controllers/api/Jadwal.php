<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('jadwal_model');
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function get_total_halaman()
	{
		$data = $this->jadwal_model->get_total_halaman()/10;
		
		if(fmod($data,1) > 0) {
			$data = $data - fmod($data,1) + 1;
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_all($halaman=1)
	{
		$limit = 10;
		$offset = ($halaman-1)*10;
		$data = $this->jadwal_model->get_all($limit, $offset);
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_for_home()
	{
		$data = $this->jadwal_model->get_for_home();
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get($id)
	{
		$data = $this->jadwal_model->get($id);
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function add()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $this->jadwal_model->add(
			$data->nama, $data->tgl_waktu, $data->alamat,
			$data->koordinat, $data->keterangan
		);
		
		echo json_encode($result);
	}
	
	public function set()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $this->jadwal_model->set(
			$data->id, $data->nama, $data->tgl_waktu,
			$data->alamat, $data->koordinat, $data->keterangan
		);
		
		echo json_encode($result);
	}
	
	public function delete($id)
	{
		header('Content-type: application/json');
		
		$result = $this->jadwal_model->delete($id);
		
		echo json_encode($result);
	}
	
}
