<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('anggota_model');
		$this->load->model('jabatan_model');
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function get_all($halaman=1)
	{
		$limit = 10;
		$offset = ($halaman-1)*10;
		$data = $this->anggota_model->get_all($limit, $offset);
		
		foreach($data as $anggota) {
			$anggota->jabatan = $this->jabatan_model->get($anggota->jabatan_id);
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_total_halaman()
	{
		$data = $this->anggota_model->get_total_halaman()/10;
		
		if(fmod($data,1) > 0) {
			$data = $data - fmod($data,1) + 1;
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get($id)
	{
		$data = $this->anggota_model->get($id);
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_for_home()
	{
		$data = $this->anggota_model->get_for_home();
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get_list_jabatan()
	{
		$data = $this->jabatan_model->get_all();
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function add()
	{
		header('Content-Type: application/json');
		
		$this->load->model('user_model');
		
		$data = json_decode(file_get_contents('php://input'));
		$id_anggota = $this->anggota_model->add(
			$data->nama, $data->jabatan_id, $data->alamat,
			$data->no_telp, $data->url_foto
		);
		$result = explode(" ", $data->nama);
		$username = $data->no_telp;
		$password = password_hash(strtolower($result[0]), PASSWORD_BCRYPT);
		$result = $this->user_model->add($id_anggota, $username, $password);
		
		echo json_encode($result);
	}
	
	public function set()
	{
		header('Content-Type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$url = "";
		
		if($data->url_foto == "") {
			$url = $this->anggota_model->get($data->id)->url_foto;
		} else {
			$url = $this->anggota_model->get($data->id)->url_foto;
			
			unlink('assets/images/profile/'.$url);
			
			$url = $data->url_foto;
		}
		
		$result = $this->anggota_model->set(
			$data->id, $data->nama, $data->jabatan_id,
			$data->alamat, $data->no_telp, $url
		);
		
		echo json_encode($result);
	}
	
	public function upload()
	{
		$to_return = new \stdClass;
		$to_return->status = false;
		$to_return->url = "";
		$target_dir = "assets/images/profile/";
		$nama_asli = pathinfo(
			$_FILES['foto']['name'], PATHINFO_FILENAME
		);
		$ekstensi = pathinfo(
			$_FILES['foto']['name'], PATHINFO_EXTENSION
		);
		$increment = 0; 
		$nama_baru = $nama_asli . '.' . $ekstensi;
		
		while(is_file($target_dir . $nama_baru)) {
			$increment++;
			$nama_baru = $nama_asli . $increment . '.' . $ekstensi;
		}
		
		$target_file = $target_dir . $nama_baru;
		
		header('Content-Type: application/json');
		
		if (move_uploaded_file(
			$_FILES["foto"]["tmp_name"], $target_file
		)) {
			$to_return->status = true;
			$to_return->url = $nama_baru;
		}
		
		echo json_encode($to_return);
	}
	
	public function delete($id)
	{
		$url = $this->anggota_model->get($id)->url_foto;
		$to_return = false;
		
		if(unlink('assets/images/profile/'.$url)) {
			if($this->anggota_model->delete($id)) {
				$to_return = true;
			}
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($to_return);
	}
}
