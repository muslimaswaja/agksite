<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visimisi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('visiMisi_model');
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function get_all($halaman=1)
	{
		$limit = 10;
		$offset = ($halaman-1)*10;
		$data = new \stdClass;
		$data->visi = $this->visiMisi_model->get_visi();
		$data->misi = $this->visiMisi_model->get_misi($limit, $offset);
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function get_for_home()
	{
		$data = new \stdClass;
		$data->visi = $this->visiMisi_model->get_visi();
		$data->misi = $this->visiMisi_model->get_misi_all();
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function get_total_halaman()
	{
		$data = $this->visiMisi_model->get_total_halaman()/10;
		
		if(fmod($data,1) > 0) {
			$data = $data - fmod($data,1) + 1;
		}
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function get($id)
	{
		$data = $this->visiMisi_model->get($id);
		
		header('Content-Type: application/json');
		
		echo json_encode($data);
	}
	
	public function add()
	{
		header('Content-Type: application/json');
		$data = json_decode(file_get_contents('php://input'));
		
		$result = $this->visiMisi_model->add($data->konten);
		
		echo json_encode($result);
	}
	
	public function set()
	{
		header('Content-Type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		
		$result = $this->visiMisi_model->set($data->id, $data->konten);
		
		echo json_encode($result);
	}
	
	public function delete($id)
	{
		header('Content-Type: application/json');
		
		$result = $this->visiMisi_model->delete($id);
		
		echo json_encode($result);
	}
}
