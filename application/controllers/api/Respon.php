<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Respon extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->model('respon_model');
		$this->load->model('transaksiRespon_model');
		
		header('Access-Control-Allow-Origin: *');
	}
	
	public function get_all()
	{
		$data = $this->transaksiRespon_model->get_all();
		
		if(count($data) > 0) {
			for($i=0; $i<count($data); $i++) {
				$data[$i]->respon = $this->respon_model->get($data[$i]->id);
			}
		}
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function get($id)
	{
		$data = $this->transaksiRespon_model->get($id);
		
		if(count($data) > 0) {
			for($i=0; $i<count($data); $i++) {
				if($data != null) {
					$data[$i]->respon = $this->respon_model->get($data[$i]->id);
				}
			}
		}
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function add()
	{
		header('Content-type: application/json');
		
		$data = json_decode(file_get_contents('php://input'));
		$result = $id_transaksi_respon = $this->transaksiRespon_model->add($data->id_kuisioner);
		
		if(isset($data->respon) && count($data->respon) > 0) {
			foreach($data->respon as $respon) {
				$result = $this->respon_model->add(
					$id_transaksi_respon, $respon->id_soal,
					$respon->respon
				);
			}
		}
		
		echo json_encode($result);
	}
	
}
