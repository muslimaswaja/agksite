<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Controller auth
*/
class Auth extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('user_model');
	}
	
	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if($this->user_model->login($username, $password)) {
			$data = array(
				'username'  => $username
			);

			$this->session->set_userdata($data);
			
			redirect('admin');
		} else {
			$_SESSION['status'] = 'false';
			$_SESSION['uname'] = $username;
			
			$this->session->mark_as_flash(array('status', 'uname'));
			
			redirect('login');
		}
	}
	
	public function logout()
	{
		$data = array('username', 'logged_in');

		$this->session->unset_userdata($data);
		
		redirect(base_url());
	}
	
}
?>
