<div id="DaftarAnggota" class="container">
	<h2 class="text-center">Daftar Anggota Karang Taruna</h2>
</div>

<div class="row justify-content-center">
	<ul class="pagination" id="pagination"></ul>
</div>

<br>

<script>
	let halaman = 1;
	let totalHalaman;
	
	function getAnggota(halaman=1) {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillAnggota(JSON.parse(this.responseText));
				setHalAktif(halaman);
			}
		};

		xhttp.open("GET", "<?= base_url('api/anggota/get_all/') ?>" + halaman, true);
		xhttp.send();
	}
	
	function getTotalHalaman() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				totalHalaman = JSON.parse(this.responseText);
				
				fillPagination(totalHalaman);
			}
		};

		xhttp.open("GET", "<?= base_url('api/anggota/get_total_halaman/') ?>", true);
		xhttp.send();
	}
	
	function fillAnggota(data) {
		if(data != null) {
			let anggotaParent = document.getElementById("DaftarAnggota");
			
			// Isi data
			data.forEach(function(anggota) {
				// Deklarasi elemen
				let anggotaRow = document.createElement("div");
				let anggotaCol = document.createElement("div");
				let imageContainer = document.createElement("img");
				let namaContainer = document.createElement("p");
				let alamatContainer = document.createElement("p");
				let jabatanContainer = document.createElement("p");
				let urlFoto = anggota.url_foto;
				let namaText = document.createTextNode(anggota.nama);
				let alamatText = document.createTextNode(anggota.alamat + " (" + anggota.no_telp + ")");
				let jabatanText = document.createTextNode(anggota.jabatan.jabatan);
				
				<?php if($this->session->has_userdata('username') != 1) { ?>
					alamatText = document.createTextNode("Gebang Kidul");
				<?php } ?>
				
				// Isi konten elemen
				anggotaRow.setAttribute("class", "row justify-content-center anggota");
				anggotaCol.setAttribute("class", "col-sm-6 biodata");
				imageContainer.setAttribute("src", "<?php echo base_url('assets/images/profile/') ?>" + anggota.url_foto);
				imageContainer.setAttribute('style', "width:64px;height:auto");
				imageContainer.className = "img-circle";
				namaContainer.setAttribute("style", "font-weight:bold");
				
				// Gabungkan elemen
				namaContainer.append(namaText);
				alamatContainer.append(alamatText);
				jabatanContainer.append(jabatanText);
				anggotaCol.append(imageContainer);
				anggotaCol.append(namaContainer);
				anggotaCol.append(alamatContainer);
				anggotaCol.append(jabatanContainer);
				anggotaRow.append(anggotaCol);
				anggotaParent.append(anggotaRow);
			});
		}
	}
	
	function fillPagination(jmlHalaman) {
		if(jmlHalaman > 0) {
			let paginationParent = document.getElementById("pagination");
			
			// Isi data
			for(let i=0; i<jmlHalaman+2; i++) {
				// Deklarasi elemen
				let paginationList = document.createElement("li");
				let paginationAnchor = document.createElement("a");
				let paginationText;
				
				if(i==0) {
					paginationList.setAttribute("onclick", "prev()");
					paginationText = document.createTextNode("Previous");
				} else if(i==jmlHalaman+1) {
					paginationList.setAttribute("onclick", "next()");
					paginationText = document.createTextNode("Next");
				} else {
					paginationList.setAttribute("onclick", "openHalaman(" + i + ")");
					paginationText = document.createTextNode(i);
				}
				
				// Isi konten elemen
				paginationList.setAttribute("class", "page-item link-pagination");
				paginationAnchor.setAttribute("class", "page-link");
				paginationAnchor.setAttribute("href", "#");
				
				// Gabungkan elemen
				paginationAnchor.append(paginationText);
				paginationList.append(paginationAnchor);
				paginationParent.append(paginationList);
			}
		}
	}
	
	function setHalAktif(halaman = 1) {
		let paginationList = document.getElementsByClassName("link-pagination");
		
		resetPagination();
		paginationList[halaman].classList.add("active");
	}
	
	function resetPagination() {
		let activePagination = document.getElementsByClassName("active");
		
		while(activePagination.length) {
			activePagination[0].classList.remove("active");
		}
	}
	
	function openHalaman(halamanTujuan) {
		halaman = halamanTujuan;
		
		resetAnggota();
		getAnggota(halaman);
	}
	
	function prev() {
		if(halaman>1) {
			resetAnggota();
			getAnggota(--halaman);
		}
	}
	
	function next() {
		if(halaman<totalHalaman) {
			resetAnggota();
			getAnggota(++halaman);
		}
	}
	
	function resetAnggota() {
		let dataAnggota = document.getElementsByClassName("anggota");
		
		while(dataAnggota.length) {
			dataAnggota[0].parentNode.removeChild(dataAnggota[0]);
		}
	}
	
	getTotalHalaman();
	getAnggota();
</script>
