
<div class="container slide" data-ride="carousel">
	<h1 class="text-center" style="margin-top: 50px; margin-bottom: 20px">Kuisioner</h1>
	<div id="kuisioner-aktif" class="container-fluid border" style="width: 80%; padding: 30px;display:none">
		<h2 id="judul-container"></h2>

		<div class="container">
			<ol type="1" id="soal-container"></ol>
			
			<a href="<?= base_url(); ?>">
				<button type="button" class="btn btn-danger btn-lg">Batal</button>
			</a>
			<button type="button" class="btn btn-primary btn-lg pull-right" onclick="kirimRespon()">Submit</button>
		</div>
	</div>

	<div id="kuisioner-nonaktif" class="container-fluid border" style="width: 80%; padding: 30px;">
		<center><h2>Saat ini masih belum ada kuisioner yang aktif!</h2></center>
	</div>

</div>

<script>
	let idKuisioner;
	
	function getKuisioner() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillKuisioner(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/kuisioner/get_active') ?>", true);
		xhttp.send();
	}
	
	function postKuisioner(data) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					window.open("<?= base_url('kuisioner/sukses'); ?>", "_self");
				}
			}
		};

		xhr.open("POST", "<?= base_url('api/respon/add') ?>", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	function fillKuisioner(data) {
		idKuisioner = data.id;
		
		if(data != null) {
			let soalParent = document.getElementById("soal-container");
			let judulContainer = document.getElementById("judul-container");
			let judulText = document.createTextNode(data.judul);
			
			judulContainer.append(judulText);
			
			document.getElementById("kuisioner-nonaktif").style.display = "none";
			document.getElementById("kuisioner-aktif").style.display = "block";
			
			// Isi soal
			data.soal.forEach(function(soal) {
				// Deklarasi elemen
				let soalContainer = document.createElement("li");
				let soalP = document.createElement("p");
				let jawabanContainer = document.createElement("form");
				let spacing = document.createElement("br");
				
				// Isi konten elemen
				let soalText = document.createTextNode(soal.soal);
				
				// Isi jawaban
				if(soal.jenis_jawaban.id == 1) {
					soal.jawaban.forEach(function(jawaban) {
						let radioContainer = document.createElement("div");
						let radioLabel = document.createElement("label");
						let radioInput = document.createElement("input");
						let radioText = document.createTextNode(jawaban.jawaban);
						let inputID = `radio-${jawaban.id}`;
						let inputName = `jawaban-id-${soal.id}`;
						
						radioContainer.setAttribute("class", "radio");
						radioInput.setAttribute("type", "radio");
						radioInput.setAttribute("name", inputName);
						radioInput.id = inputID;
						
						radioLabel.append(radioInput);
						radioLabel.append(radioText);
						radioContainer.append(radioLabel);
						jawabanContainer.append(radioContainer);
					});
				} else if(soal.jenis_jawaban.id == 2) {
					let jawabanInput = document.createElement("input");
					let inputID = `jawaban-id-${soal.id}`;
					
					jawabanInput.id = inputID;
					jawabanInput.setAttribute("type", "text");
					jawabanInput.setAttribute("class", "form-control border-top-0 border-right-0 border-left-0");
					
					jawabanContainer.append(jawabanInput);
				} else if(soal.jenis_jawaban.id == 3) {
					soal.jawaban.forEach(function(jawaban) {
						let checkboxContainer = document.createElement("div");
						let checkboxLabel = document.createElement("label");
						let checkboxInput = document.createElement("input");
						let whiteSpace = document.createTextNode(String.fromCharCode(160) + " ");
						let checkboxText = document.createTextNode(jawaban.jawaban);
						let inputID = `checkbox-${jawaban.id}`;
						let inputName = `jawaban-id-${soal.id}`;
						
						checkboxContainer.setAttribute("class", "form-check");
						checkboxInput.setAttribute("type", "checkbox");
						checkboxInput.setAttribute("class", "form-check-input");
						checkboxInput.setAttribute("name", inputName);
						checkboxInput.id = inputID;
						checkboxLabel.setAttribute("for", inputID);
						checkboxLabel.setAttribute("class", "form-check-label");
						
						checkboxLabel.append(whiteSpace);
						checkboxLabel.append(checkboxText);
						checkboxContainer.append(checkboxInput);
						checkboxContainer.append(checkboxLabel);
						jawabanContainer.append(checkboxContainer);
					});
				}
				
				// Gabungkan elemen
				soalP.append(soalText);
				soalContainer.append(soalP);
				soalContainer.append(jawabanContainer);
				soalContainer.append(spacing);
				soalParent.append(soalContainer);
			});
		}
	}
	
	function kirimRespon() {
		let data = {
			id_kuisioner: idKuisioner,
			respon: []
		}
		
		let soalContainer = document.getElementById("soal-container")
			.childNodes;
		
		for(let i=0; i<soalContainer.length; i++) {
			let inputJawaban = soalContainer[i].childNodes[1].childNodes[0];
			let className = inputJawaban.className;
			
			if(className == "radio") {
				let jmlJawaban = inputJawaban.parentNode
					.childNodes.length;
				
				for(let j=0; j<jmlJawaban; j++) {
					let jawabanContainer = inputJawaban.parentNode.childNodes[j]
						.childNodes[0];
					
					if(jawabanContainer.childNodes[0].checked == true) {
						let idSoal = jawabanContainer.childNodes[0].name
							.replace("jawaban-id-","");
						let jawaban = jawabanContainer.childNodes[1].data;
						
						data.respon.push({
							id_soal: idSoal,
							respon: jawaban
						});
					}
				}
			} else if(className == "form-check") {
				let jmlJawaban = inputJawaban.parentNode
					.childNodes.length;
					
				for(let j=0; j<jmlJawaban; j++) {
					let jawabanContainer = inputJawaban.parentNode.childNodes[j]
						.childNodes[0];
					
					if(jawabanContainer.checked == true) {
						let idSoal = jawabanContainer.name
							.replace("jawaban-id-","");
						let jawaban = jawabanContainer.parentNode
							.childNodes[1].childNodes[1].data;
						
						data.respon.push({
							id_soal: idSoal,
							respon: jawaban
						});
					}
				}
			} else {
				let idSoal = inputJawaban.id.replace("jawaban-id-","");
				
				data.respon.push({
					id_soal: idSoal,
					respon: inputJawaban.value
				});
			}
		}
		
		postKuisioner(data);
	}
	
	getKuisioner();
</script>
