
<div id="KegiatanCarousel" class="carousel slide" data-ride="carousel">

	<!-- Indicators -->
	<ul class="carousel-indicators" id="kegiatan-indicator"></ul>

	<!-- The slideshow -->
	<div class="carousel-inner" id="kegiatan-slideshow"></div>
	<!-- Left and right controls -->
	<a class="carousel-control-prev" href="#KegiatanCarousel" data-slide="prev">
		<span class="carousel-control-prev-icon"></span>
	</a>

	<a class="carousel-control-next" href="#KegiatanCarousel" data-slide="next">
		<span class="carousel-control-next-icon"></span>
	</a>
</div>


<!-- Container (Jadwal Section) -->
<div id="JadwalKegiatan" class="container">
	<h2 class="text-center">Jadwal Kegiatan</h2>
	<div id="text-carousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ul class="carousel-indicators">
			
		</ul>
		<!-- Wrapper for slides -->
		<div class="row">
			<div class="carousel-inner">
				
			</div>
		</div>
		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#text-carousel" data-slide="prev">
			<span class="carousel-control-prev-icon"></span>
		</a>
		<a class="carousel-control-next" href="#text-carousel" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		</a>
	</div>
</div>

<!-- Container (VisiMisi Section) -->
<div id="visimisi" class="bg-1">
	<div class="container">
		<h2>Visi:</h2>
		<p class="text-content" id="visi-container"></p>
		<h2>Misi:</h2>
		<ul class="text-content" id="misi-container"></ul>
	</div>
</div>

<!-- Container (Pengurus Section) -->
<div id="pengurus" class="container">
	<h2 class="text-center">Pengurus Kartar</h2>

	<div class="row justify-content-center" id="pengurus-container"></div>
</div>

<!-- Container (Kontak Section) -->
<div id="kontak" class="bg-1">
	<div class="container text-center">
		<h2>Kontak</h2>
		<p class="text-content">
			<span class="fa fa-phone"></span> :
			<span id="no-telp-container"></span>
		</p>
		<p class="text-content">
			<span class="fa fa-map-marker"></span> :
			<span id="alamat-container"></span>
		</p>
		<p class="text-content">
			<span class="fa fa-envelope"></span> :
			<span id="email-container"></span>
		</p>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="rincian-kegiatan" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			
			<div class="modal-body">
				<h2 id="rincian-judul"></h2>
				<h4 id="rincian-waktu"></h4>
				<br>

				<h2>Detail Kegiatan</h2>
				<h5 id="rincian-keterangan"></h5>
				<br>

				<h2>Tempat Kegiatan</h2>
				<h5 id="rincian-alamat"></h5>
				<br>
				<div id="map" style="width:100%;height:400px;"></div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>

<script>

	function myMap(lat, lng, namaLokasi) {
		var mapCanvas = document.getElementById("map");
		let latLng = {
			lat: parseFloat(lat),
			lng: parseFloat(lng)
		};
		
		var mapOptions = {
			center: latLng,
			zoom: 16
		};
		
		var map = new google.maps.Map(mapCanvas, mapOptions);
		var marker = new google.maps.Marker({
			position: latLng,
			map: map,
			title: namaLokasi
		});
	}

	function getGambarKegiatan() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				fillGambarKegiatan(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/gambar/get_for_home') ?>", true);
		xhttp.send();
	}

	function getJadwalKegiatan() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				fillJadwalKegiatan(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/jadwal/get_for_home') ?>", true);
		xhttp.send();
	}

	function getRincianJadwal(id) {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillRincianJadwal(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/jadwal/get/') ?>" + id, true);
		xhttp.send(); 
	}

	function getVisiMisi() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				fillVisiMisi(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/visimisi/get_for_home') ?>", true);
		xhttp.send();
	}

	function getPengurus() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				fillPengurus(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/anggota/get_for_home') ?>", true);
		xhttp.send();
	}

	function getKontak() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				fillKontak(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/kontak/get') ?>", true);
		xhttp.send();
	}

	function fillGambarKegiatan(listGambar) {
		let index = 0;

		listGambar.forEach(function (gambar) {
			// Buat elemen untuk indikator carousel
			let containerIndicator = document.getElementById("kegiatan-indicator");
			let indicator = document.createElement("li");

			// Buat elemen untuk inner carousel
			let containerSlideshow = document.getElementById("kegiatan-slideshow");
			let slideshow = document.createElement("div");
			let slideImage = document.createElement("img");

			// Set atribut indikator carousel
			indicator.setAttribute('data-target', '#KegiatanCarousel');
			indicator.setAttribute('data-slide-to', index);

			// Set atribut inner carousel
			slideshow.className = "carousel-item";
			slideImage.setAttribute('src', "<?= base_url('assets/images/'); ?>" + gambar.url);
			slideImage.setAttribute('alt', gambar.keterangan);

			if (index == 0) {
				indicator.className = "active";
				slideshow.className = "carousel-item active";
			}

			// Menambahkan indikator ke container indikator carousel
			containerIndicator.appendChild(indicator);

			// Menambahkan slideshow ke container inner carousel
			slideshow.appendChild(slideImage);
			containerSlideshow.appendChild(slideshow);

			index++;
		});
	}

	function fillJadwalKegiatan(listJadwal) {
		let jadwalParent = document.querySelector("#JadwalKegiatan .carousel-inner");
		let indicator = document.querySelector("#text-carousel .carousel-indicators");
		// Indicator
		for (let index = 0; index < listJadwal.length / 3; index++) {
			// Buat elemen untuk jadwal kegiatan
			let carouselItem = document.createElement("div");
			let carouselContent = document.createElement("div");
			let carouselList = document.createElement("li");

			// Set atribut container jadwal dan item
			carouselList.setAttribute("data-target", "#text-carousel");
			carouselList.setAttribute("data-slide-top", index);
			if(index == 0){
				carouselList.className = "active";
				carouselItem.className = "carousel-item active";
			}
			else{
				carouselItem.className = "carousel-item";
			}
			carouselContent.className = "carousel-content";

			indicator.appendChild(carouselList);

			let jadwalIsi = listJadwal.slice(index, index + 3);
			
			jadwalIsi.forEach(function (jadwal) {
				let jadwalContainer = document.createElement("div");
				let waktuContainer = document.createElement("p");
				let lokasiContainer = document.createElement("p");
				let acaraContainer = document.createElement("p");

				jadwalContainer.className =  "col-sm-4 isi-cont text-content text-center";
				jadwalContainer.setAttribute("style","cursor:pointer");
				jadwalContainer.setAttribute("onclick",`getRincianJadwal(${jadwal.id})`);
				jadwalContainer.setAttribute("data-toggle","modal");
				jadwalContainer.setAttribute("data-target","#rincian-kegiatan");
				
				// Buat konten untuk jadwal kegiatan
				let tgl_waktu = jadwal.tgl_waktu.split(" ");
				let tanggal = formatTanggal(tgl_waktu[0]);
				let jam = tgl_waktu[1].substr(0, 5) + " WIB";
				let waktu = document.createTextNode(`${tanggal} (${jam})`);
				let lokasi = document.createTextNode(jadwal.alamat);
				let acara = document.createTextNode(jadwal.nama);

				// Menambahkan jadwal ke container jadwal
				waktuContainer.appendChild(waktu);
				lokasiContainer.appendChild(lokasi);
				acaraContainer.appendChild(acara);
				jadwalContainer.appendChild(waktuContainer);
				jadwalContainer.appendChild(lokasiContainer);
				jadwalContainer.appendChild(acaraContainer);
				carouselContent.appendChild(jadwalContainer);
			});
			
			carouselItem.appendChild(carouselContent);
			jadwalParent.appendChild(carouselItem);
		}
	}

	function fillRincianJadwal(data) {
		let containerJudul = document.getElementById("rincian-judul");
		let containerWaktu = document.getElementById("rincian-waktu");
		let containerKeterangan = document.getElementById("rincian-keterangan");
		let containerAlamat = document.getElementById("rincian-alamat");
		let tgl_waktu = data.tgl_waktu.split(" ");
		let tanggal = formatTanggal(tgl_waktu[0]);
		let jam = tgl_waktu[1].substr(0, 5) + " WIB";
		let waktu = `${tanggal} - ${jam}`;
		let latLng = data.koordinat.split(" ");
		
		containerJudul.innerHTML = data.nama;
		containerWaktu.innerHTML = waktu;
		containerKeterangan.innerHTML = data.keterangan;
		containerAlamat.innerHTML = data.alamat;
		
		myMap(latLng[0], latLng[1], data.alamat);
	}

	function fillPengurus(pengurus) {
		// Buat elemen untuk pengurus
		let pengurusParent = document.getElementById("pengurus-container");

		pengurus.forEach(function (pengurus) {
			let pengurusContainer = document.createElement("div");
			let fotoContainer = document.createElement("img");
			let breaker = document.createElement("br");
			let breaker2 = document.createElement("br");
			let namaParent = document.createElement("p");
			let namaContainer = document.createElement("b");
			let jabatanParent = document.createElement("p");
			let jabatanContainer = document.createElement("b");

			pengurusContainer.className = "col-sm-2 isi-cont text-center text-content";
			fotoContainer.setAttribute('src', "<?= base_url('assets/images/profile/'); ?>" + pengurus.url_foto);
			fotoContainer.setAttribute('style', "width:64px;height:auto");
			fotoContainer.className = "img-circle";

			let nama = document.createTextNode(pengurus.nama);
			let jabatan = document.createTextNode(pengurus.jabatan);

			namaContainer.appendChild(nama);
			jabatanContainer.appendChild(jabatan);
			namaParent.appendChild(namaContainer);
			jabatanParent.appendChild(jabatanContainer);

			pengurusContainer.appendChild(fotoContainer);
			pengurusContainer.appendChild(breaker);
			pengurusContainer.appendChild(breaker2);
			pengurusContainer.appendChild(namaParent);
			pengurusContainer.appendChild(jabatanParent);

			pengurusParent.appendChild(pengurusContainer);
		});
	}

	function fillVisiMisi(visiMisi) {
		// Buat elemen untuk visi misi
		let visiContainer = document.getElementById("visi-container");
		let misiListContainer = document.getElementById("misi-container");
		let visi = document.createTextNode(visiMisi.visi);

		visiMisi.misi.forEach(function (misi) {
			let misiContainer = document.createElement("li");
			let misiText = document.createTextNode(misi.konten);

			misiContainer.appendChild(misiText);
			misiListContainer.appendChild(misiContainer);
		});

		// Tampilkan visi
		visiContainer.appendChild(visi);
	}

	function fillKontak(kontak) {
		let noTelpContainer = document.getElementById("no-telp-container");
		let alamatContainer = document.getElementById("alamat-container");
		let emailContainer = document.getElementById("email-container");

		let noTelp = document.createTextNode(kontak.telepon);
		let alamat = document.createTextNode(kontak.alamat);
		let email = document.createTextNode(kontak.email);

		noTelpContainer.appendChild(noTelp);
		alamatContainer.appendChild(alamat);
		emailContainer.appendChild(email);
	}

	function formatTanggal(tanggal) {
		let elemenTanggal = tanggal.split("-");
		let hari = elemenTanggal[2];
		let bulan = elemenTanggal[1];
		let tahun = elemenTanggal[0];
		let toReturn = "";

		switch (bulan) {
			case '01':
				bulan = "Januari";
				break;
			case '02':
				bulan = "Februari";
				break;
			case '03':
				bulan = "Maret";
				break;
			case '04':
				bulan = "April";
				break;
			case '05':
				bulan = "Mei";
				break;
			case '06':
				bulan = "Juni";
				break;
			case '07':
				bulan = "Juli";
				break;
			case '08':
				bulan = "Agustus";
				break;
			case '09':
				bulan = "September";
				break;
			case '10':
				bulan = "Oktober";
				break;
			case '11':
				bulan = "November";
				break;
			case '12':
				bulan = "Desember";
				break;
		}

		toReturn = `${hari} ${bulan} ${tahun}`;

		return toReturn;
	}

	getGambarKegiatan();
	getJadwalKegiatan();
	getPengurus();
	getVisiMisi();
	getKontak();
	//~ getRincianJadwal(3);
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4dgysO-WFccOuWT6UDfFwUkGgibjV5UY&callback=myMap" async defer></script>
