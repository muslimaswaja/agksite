<div class="container slide" data-ride="carousel">
	<h1 class="text-center" style="margin-top: 50px; margin-bottom: 20px">Terima kasih telah mengisi kuisioner</h1>
	<br>
	<h4 class="text-center">Data berhasil disimpan. Terima kasih telah berpartisipasi dalam kuisioner ini.</h4>
	<a href="<?= base_url('kuisioner'); ?>">
		<h4 class="text-center">Klik disini untuk mengisi respon kuisioner kembali</h4>
	</a>
</div>