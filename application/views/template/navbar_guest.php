<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Theme Made By www.w3schools.com - No Copyright -->

		<title>AGKSite</title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/guest/css/style.css') ?>">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	</head>

	<style type="text/css">
		body {
			font-size: 14px;
		}
	</style>

	<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
		<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
			<a class="navbar-brand" href="#">AGKSite</a>
			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link active" href="<?php echo base_url(); ?>">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('anggota'); ?>">Daftar Anggota</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('kuisioner'); ?>">Kuisioner</a>
					</li>
					<li class="nav-item">
						<?php if($this->session->has_userdata('username') == 1) { ?>
							<a class="nav-link" href="<?php echo base_url('admin'); ?>">Admin</a>
						<?php } else { ?>
							<a class="nav-link" href="<?php echo base_url('login'); ?>">Login</a>
						<?php } ?>
					</li>    
				</ul>
			</div>
		</nav>
