<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<title>AdminLTE 2 | Blank Page</title>
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<link rel="stylesheet" href="<?php echo base_url('assets/w3.css') ?>" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Navbar -->
	<div class="w3-bar w3-light-grey">
	  <a href="#" class="w3-bar-item w3-button">Home</a>
	  <a href="#" class="w3-bar-item w3-button">Link 1</a>
	  <a href="#" class="w3-bar-item w3-button">Link 2</a>
	  <a href="#" class="w3-bar-item w3-button w3-green w3-right">Link 3</a>
	</div>
</body>
</html>