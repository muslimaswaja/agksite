<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content" style="background-color: #ecf0f5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Anggota
        <small>Tambah anggota karang taruna</small>
      </h1> 
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-9">
                    <input id="nama-input" type="text" class="form-control" placeholder="Masukkan Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jabatan</label>
                  <div class="col-sm-9">
                    <select id="jabatan-input" class="form-control"></select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-9">
                    <textarea id="alamat-input" class="form-control" rows="3" placeholder="Masukkan Alamat"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">No.Telp</label>
                  <div class="col-sm-9">
                    <input id="no-telp-input" type="tel" class="form-control" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="exampleInputFile">File input</label>
                  <input id="foto-input" type="file" onchange="fillInfo()">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-default">Cancel</button>
                <button onclick="postFoto()" type="button" class="btn btn-primary pull-right">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
	// Deklarasi variabel
	let formData;
	
	// API GET
	function getListJabatan() {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillListJabatan(JSON.parse(this.responseText));
			}
		};

		xhr.open("GET", "<?= base_url('api/anggota/get_list_jabatan') ?>", true);
		xhr.send();
	}
	
	// API POST
	function postFoto() {
		if(formData.getAll('foto').length > 0) {
			let xhr = new XMLHttpRequest();
		
			xhr.onload = function () {
			  if (xhr.status === 200) {
				let data = JSON.parse(this.responseText);
				formData = new FormData();
				
				if(data.status) {
					simpanDataAnggota(data.url);
				}
			  }
			};
			
			xhr.open('POST', '<?= base_url(); ?>api/anggota/upload', true);
			xhr.send(formData);
		} else {
			simpanDataAnggota();
		}
	}
	
	function postDataAnggota(data) {
		let xhr = new XMLHttpRequest();
	
		xhr.onload = function () {
			if (xhr.status === 200) {
				if(JSON.parse(this.responseText)) {
					window.open("<?= base_url('admin/anggota/simpan'); ?>","_self");
				}
			}
		};
		
		xhr.open('POST', '<?= base_url(); ?>api/anggota/add', true);
		xhr.send(JSON.stringify(data));
	}
	
	// Content filler
	function fillInfo() {
		let inputFile;
		formData = new FormData();
		
		inputFile = document.getElementById("foto-input");
		
		if('files' in inputFile) {
			if(inputFile.files.lengths != 0) {
				for(
					let i = 0; i < inputFile.files.length; i++
				) {
					let file  = inputFile.files[i];
					
					if('name' in file && isImage(file.name)) {
						if(
							'size' in file
							&& isSizeOk(file.size)
						) {
							formData.append(
								'foto',
								file,
								file.name
							);
						}
					}
				}
			}
		}
	}
	
	function fillListJabatan(data) {
		if(data.length > 0) {
			let listJabatanSelect = document.getElementById("jabatan-input");
			
			data.forEach(function(jabatan) {
				let jabatanOption = document.createElement("option");
				let jabatanText = document.createTextNode(jabatan.jabatan);
				
				jabatanOption.setAttribute("value",jabatan.id);
				jabatanOption.append(jabatanText);
				listJabatanSelect.append(jabatanOption);
			});
		}
	}
	
	// Fungsi lain
	function simpanDataAnggota(foto = "") {
		let nama = document.getElementById("nama-input").value;
		let jabatan = document.getElementById("jabatan-input").value;
		let alamat = document.getElementById("alamat-input").value;
		let noTelp = document.getElementById("no-telp-input").value;
		let urlFoto = foto;
		
		let data = {
			nama: nama,
			jabatan_id: jabatan,
			alamat: alamat,
			no_telp: noTelp,
			url_foto: urlFoto
		};
		
		postDataAnggota(data);
	}
	
	function isImage(name) {
		let toReturn = false;
		let extension = name.split(".").pop().toLowerCase();
		
		if(
			extension == 'jpg' || extension == 'png'
			|| extension == 'jpeg'
		) {
			toReturn = true;
		}
		
		return toReturn;
	}
	
	function isSizeOk(size) {
		let toReturn = false;
		
		if(size <= 2000000) {
			toReturn = true;
		}
		
		return toReturn;
	}
	
	getListJabatan();
</script>

<?php
$this->load->view('template/footer');
?>
