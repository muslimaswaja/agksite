<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->

<style type="text/css">
	.active {
		background-color: #F39C12;
		color: #fff;
	}

	.entry:not(:first-of-type) {
		margin-top: 10px;
	}

	.glyphicon {
		font-size: 12px;
	}
</style>

<!-- Content Wrapper. Contains page content -->
<div id="KuisionerBox" class="content" style="background-color: #ecf0f5">
	<form role="form" action="#">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<!-- Edit (Nama Kuisioner) -->
			<small>Berikan soalnya disini</small>
		</h1>
		<br>

	</section>

	<!-- Main content -->
		<section class="content" id="main-content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Pertanyaan</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool btn-info btn-s" style="color: #fff" onclick="simpanKuisioner()">Simpan</button>
					</div>
				</div>

				<div class="box-footer text-right tambah-pertanyaan">
					<button type="button" class="btn btn-box-tool btn-default btn-s btn-tambah-pertanyaan">Tambah Pertanyaan</button>
				</div>
			</div>
			<div class="box-footer">
				Footer
			</div>
			<!-- /.box-footer-->
		</section>
	</form>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<script>
	function getKuisioner() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillKuisioner(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/kuisioner/get/'.$id) ?>", true);
		xhttp.send();
	}
	
	function fillKuisioner(data) {
		console.log(data);
		display_judul();
		
		document.getElementById("judul-kuisioner").value = data.judul;
		
		let noPertanyaan = 1;
		
		if(data.soal != null) {
			data.soal.forEach(function(pertanyaan) {
				buat_pertanyaan();
				var parent_index = $(".box-content:last").parentsUntil(".main-content", ".cont-pertanyaan").index() - 1;
				display_pilihan(parent_index, "radio");
				
				let pertanyaanInput = document
					.getElementById("box-" + noPertanyaan).childNodes[0]
					.childNodes[1].childNodes[0].childNodes[0];
				let pertanyaanJenis = document
					.getElementById("box-" + noPertanyaan).childNodes[0]
					.childNodes[1].childNodes[1].childNodes[0]
					.childNodes[pertanyaan.id_jenis_jawaban-1];
				pertanyaanInput.value = pertanyaan.soal;
				pertanyaanJenis.selected = true;
				
				if(pertanyaan.id_jenis_jawaban == 2) {
					ganti_tipe(noPertanyaan-1, "text");
				} else if(pertanyaan.id_jenis_jawaban == 1) {
					ganti_tipe(noPertanyaan-1, "radio");
				} else if(pertanyaan.id_jenis_jawaban == 3) {
					ganti_tipe(noPertanyaan-1, "checkbox");
				}
				
				let indeksPilihan = 0;
				
				if(pertanyaan.id_jenis_jawaban != 2) {
					pertanyaan.jawaban.forEach(function(jawaban) {
						var tipe = $(".cont-pertanyaan:eq(" + parent_index + ") .TanyaJawab .tipe-soal").val();
						
						if(indeksPilihan > 0) {
							display_pilihan(noPertanyaan-1, tipe);
						}
						
						let jawabanInput = document
							.getElementById("box-" + noPertanyaan)
							.childNodes[0].childNodes[1]
							.childNodes[indeksPilihan+2].childNodes[1]
							.childNodes[0];
							
						jawabanInput.value = jawaban.jawaban;
						
						console.log(jawabanInput);
						
						indeksPilihan++;
					});
				}
				
				noPertanyaan++;
			});
		}
	}
	
	function simpanKuisioner() {
		let idKuisioner = <?= $id; ?>;
		let judulKuisioner = document.getElementById("judul-kuisioner").value;
		let daftarSoal = [];
		let containerSoal = document.getElementsByClassName("cont-pertanyaan");
		let data;
		
		for(let i=0; i<containerSoal.length; i++) {
			let tipeSoal = containerSoal[i].childNodes[0].childNodes[1]
				.childNodes[1].childNodes[0];
			let pertanyaan = containerSoal[i].childNodes[0]
				.childNodes[1].childNodes[0].childNodes[0].value;
			let containerJawaban = containerSoal[i].childNodes[0]
				.childNodes[1];
			let idTipeSoal;
			let jawaban = [];
			let jmlJawaban = containerJawaban.childNodes.length - 2;
			
			if(tipeSoal.value == "radio") {
				idTipeSoal = 1;
			} else if(tipeSoal.value == "text") {
				idTipeSoal = 2;
			} else if(tipeSoal.value == "checkbox") {
				idTipeSoal = 3;
			}
			
			if(idTipeSoal != 2) {
				for(let j=0; j<jmlJawaban; j++) {
					jawaban.push({
						jawaban: containerJawaban.childNodes[j+2]
							.childNodes[1].childNodes[0].value
					});
				}
				
				daftarSoal.push({
					id_jenis_jawaban: idTipeSoal,
					soal: pertanyaan,
					jawaban: jawaban
				});
			} else {
				daftarSoal.push({
					id_jenis_jawaban: idTipeSoal,
					soal: pertanyaan
				});
			}
		}
		
		data = {
			id: idKuisioner,
			judul: judulKuisioner,
			soal: daftarSoal
		};
		
		kirimData(data);
	}
	
	function kirimData(data) {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					window.open("<?= base_url('admin/kuisioner'); ?>", "_self");
				}
			}
		};

		xhr.open("PUT", "<?= base_url('api/kuisioner/set') ?>", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	getKuisioner();
</script>

<?php
$this->load->view('template/footer');
?>
