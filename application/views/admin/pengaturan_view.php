<?php
$this->load->view('template/navbar');
?>

  <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content" style="background-color: #ecf0f5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pengaturan
        <small>untuk tampilan halaman depan website karang taruna Gebang Kidul</small>
      </h1> 
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" onclick="halaman=1;nomor=1">Visi Misi</a></li>
              <li><a href="#tab_2" data-toggle="tab" onclick="halaman=1;nomor=1">Gambar Kegiatan</a></li>
              <li><a href="#tab_3" data-toggle="tab">Kontak</a></li>
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <div style="margin-bottom:10px">
                  <button type="button" class="btn btn-info btn-s" data-toggle="modal" data-target="#tambah-modal-misi">Tambah Misi</button>
                </div>
                
                <!-- Konfirmasi data ditambahkan -->
				<div id="notif-berhasil" class="alert alert-success alert-dismissible" style="display:none">
					<button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i> Data berhasil disimpan.
				</div>
				
				<!-- Konfirmasi data dihapuss -->
				<div id="notif-del-berhasil" class="alert alert-danger alert-dismissible" style="display:none">
					<button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i> Data berhasil dihapus.
				</div>

                <table class="table table-hover" style="word-wrap:break;">
				<thead>
					<tr>
					  <th>No</th>
					  <th>Jenis</th>
					  <th style="width:80%">Konten</th>
					  <th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					  <td></td>
					  <td>Visi</td>
					  <td id="visi-container"></td>
					  <td>
						<button onclick="getVMData(1)" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#visimisi-edit">Edit</button>
					  </td>
					</tr>
				</tbody>
              </table>
              <div class="box-footer text-center">
                <ul class="pagination" id="pagination" style="display:none"></ul>
              </div>
              </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane" id="tab_2">

                <div style="margin-bottom:10px">
                  <button type="button" class="btn btn-info btn-s" data-toggle="modal" data-target="#tambah-gambar">Tambah Gambar</button>
                </div>

                <!-- Konfirmasi tambah gambar -->
                <div id="notif-tambah-gambar" class="alert alert-success alert-dismissible" style="display:none">
                  <button type="button" class="close" aria-hidden="true" onclick="hideAlert(this)">&times;</button>
                  <i class="icon fa fa-check"></i> Gambar berhasil ditambahkan.
                </div>
                <!-- Konfirmasi hapus gambar -->
                <div id="notif-del-gambar" class="alert alert-danger alert-dismissible" style="display:none">
                  <button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
                  <i class="icon fa fa-check"></i> Gambar berhasil dihapus.
                </div>
                <!-- Konfirmasi hapus gambar -->
                <div id="notif-edit-gambar" class="alert alert-success alert-dismissible" style="display:none">
                  <button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
                  <i class="icon fa fa-check"></i> Gambar berhasil diedit.
                </div>

                <!-- Keterangan kalo isi tabel kosong -->
                <div id="data-empty-gambar" class="callout callout-warning">
                  <h4>Data Kosong</h4>
                  <p>Klik <a href="#" data-toggle="modal" data-target="#tambah-gambar">sini</a> untuk menambahkan data</p>
                </div>
                <!-- Akhir keterangan -->

                <table id="data-table-gambar" class="table table-hover" style="display:none">
					<thead>
						<tr>
						  <th>No</th>
						  <th>Nama File</th>
						  <th>Keterangan</th>
						  <th>Gambar</th>
						  <th>Aksi</th>
						</tr>
					</thead>
					
					<tbody></tbody>
              </table>
              <div class="box-footer text-center">
                <ul class="pagination" id="pagination-gambar" style="display:none"></ul>
              </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
				<!-- Konfirmasi data ditambahkan -->
				<div id="notif-berhasil-kontak" class="alert alert-success alert-dismissible" style="display:none">
					<button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
					<i class="icon fa fa-check"></i> Data berhasil disimpan.
				</div>
				
                <form role="form" class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon</label>
                        <div class="col-sm-9">
                          <input id="no-telp" type="tel" class="form-control" placeholder="Masukkan Telepon">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-9">
                          <textarea id="alamat" class="form-control" rows="3" placeholder="Masukkan Alamat"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-9">
                          <input id="email" type="email" class="form-control" placeholder="Masukkan Nama">
                        </div>
                      </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                      <button type="button" onclick="simpanKontak()" class="btn btn-primary pull-right">Submit</button>
                    </div>
                  </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->

    </section>

    <!-- Modal tambah misi -->
    <div class="modal fade" id="tambah-modal-misi">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				
				<h4 class="modal-title">Tambah Misi</h4>
				
				</div>
				<div class="modal-body">
					<form role="form" class="form-horizontal">
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">Konten</label>
								
								<div class="col-sm-9">
									<input id="input-misi-baru" type="text" class="form-control" placeholder="Masukkan Konten">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button data-dismiss="modal" type="button" class="btn btn-default">Cancel</button>
							<button data-dismiss="modal" onclick="tambahMisi()" type="button" class="btn btn-primary pull-right">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- akhir modal tambah misi -->

    <div class="modal fade" id="visimisi-edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					
					<h4 class="modal-title">Edit Misi</h4>
				</div>
				
				<div class="modal-body">
					<form role="form" class="form-horizontal">
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">Konten</label>
								<div class="col-sm-9">
									<input id="vm-container" type="text" class="form-control" placeholder="Masukkan Konten">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button data-dismiss="modal" type="button" class="btn btn-default">Cancel</button>
							<button data-dismiss="modal" type="button" onclick="simpanVisiMisi()" class="btn btn-primary pull-right">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal edit misi-->
	
	<!-- Modal Hapus-->
	<div class="modal fade" id="visimisi-hapus">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					
					<h4 class="modal-title">Hapus Data ?</h4>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteVisiMisi()">Hapus</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

        <!-- Modal tambah gambar -->
        <div class="modal fade" id="tambah-gambar">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Gambar</h4>
              </div>
              <div class="modal-body">
              <form role="form" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-9">
                    <input id="keterangan-baru" type="text" class="form-control" placeholder="Masukkan Keterangan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="gambar">Input gambar</label>
                  <div class="col-sm-9">
                    <input type="file" id="gambar" onchange="fillInfo()">
                    <p class="help-block">Masukkan gambar disini.</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="button" onclick="upload()" data-dismiss="modal" class="btn btn-primary pull-right">Submit</button>
              </div>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal tambah gambar-->

        <!-- Modal edit gambar -->
        <div class="modal fade" id="edit-gambar">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Gambar</h4>
              </div>
              <div class="modal-body">
              <form role="form" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-9">
                    <input id="gk-edit" type="text" class="form-control" placeholder="Masukkan Keterangan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="gambar-edit">Input gambar</label>
                  <div class="col-sm-9">
                    <input type="file" id="gambar-edit" onchange="fillInfo(true)">
                    <p class="help-block">Masukkan gambar disini.</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="button" onclick="upload(true)" data-dismiss="modal" class="btn btn-primary pull-right">Submit</button>
              </div>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal edit gambar-->
        
        <!-- Modal Hapus-->
		<div class="modal fade" id="gambar-hapus">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						
						<h4 class="modal-title">Hapus Data?</h4>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteGambar()">Hapus</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
	// Deklarasi variabel
	let halaman = 1;
	let nomor = 1;
	let totalHalaman;
	let idToEdit;
	let idToDelete;
	let formData = new FormData();
	
	// API GET
	function getVisiMisi(halaman=1) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				nomor = ((halaman - 1) * 10) + 1;
				
				fillVisiMisi(JSON.parse(this.responseText));
				setHalAktif(halaman);
			}
		};

		xhr.open("GET", "<?= base_url('api/visimisi/get_all/') ?>" + halaman, true);
		xhr.send();
	}
	
	function getGambar(halaman=1) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				nomor = ((halaman - 1) * 10) + 1;
				
				fillGambar(JSON.parse(this.responseText));
				setHalAktif(halaman, true);
			}
		};

		xhr.open("GET", "<?= base_url('api/gambar/get_all/') ?>" + halaman, true);
		xhr.send();
	}
	
	function getKontak() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				fillKontak(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/kontak/get') ?>", true);
		xhttp.send();
	}
	
	function getVMData(id) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillVMData(JSON.parse(this.responseText));
			}
		};

		xhr.open("GET", "<?= base_url('api/visimisi/get/') ?>" + id, true);
		xhr.send();
	}
	
	function getGambarDatum(id) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillGambarEdit(JSON.parse(this.responseText));
			}
		};

		xhr.open("GET", "<?= base_url('api/gambar/get/') ?>" + id, true);
		xhr.send();
	}
	
	function getTotHalVM(isDelete = false) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				totalHalaman = JSON.parse(this.responseText);
				
				if(isDelete) {
					if(halaman > totalHalaman) {
						prev();
					} else {
						openHalaman(halaman);
					}
				}
				
				fillPagination(totalHalaman);
			}
		};

		xhr.open("GET", "<?= base_url('api/visimisi/get_total_halaman') ?>", true);
		xhr.send();
	}

	function getTotHalGambar(isDelete = false) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				totalHalaman = JSON.parse(this.responseText);
				
				if(isDelete) {
					if(halaman > totalHalaman) {
						prev(true);
					} else {
						openHalaman(halaman, true);
					}
				}
				
				fillPagination(totalHalaman, true);
			}
		};

		xhr.open("GET", "<?= base_url('api/gambar/get_total_halaman') ?>", true);
		xhr.send();
	}
	
	// API POST
	function upload(isEdit = false) {
		if(formData.getAll('gambar').length > 0) {
			let xhr = new XMLHttpRequest();
		
			xhr.onload = function () {
			  if (xhr.status === 200) {
				let data = JSON.parse(this.responseText);
				formData = new FormData();
				
				if(data.status) {
					if(isEdit) {
						putDataGambar(data.url, true);
					} else {
						postDataGambar(data.url);
					}
				}
			  }
			};
			
			xhr.open('POST', '<?= base_url(); ?>api/gambar/upload', true);
			xhr.send(formData);
		} else {
			if(isEdit) {
				putDataGambar("", true);
			} else {
				postDataGambar("");
			}
		}
	}
	
	function kirimMisi(data) {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-berhasil").style.display = "block";
					
					resetMisi();
					deletePagination();
					getTotHalVM(true);
				}
			}
		};

		xhr.open("POST", "<?= base_url('api/visimisi/add') ?>", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	function postDataGambar(nama) {
		let xhr = new XMLHttpRequest();
		let keterangan = document.getElementById("keterangan-baru").value;
		let data = {
			"url" : nama,
			"keterangan" : keterangan
		};
		
		xhr.onload = function () {
			if (xhr.status === 200) {
				deletePagination(true);
				getTotHalGambar();
				resetGambar();
				getGambar(halaman);
				
				document.getElementById("notif-tambah-gambar").style.display = null;
			}
		};
		
		xhr.open('POST', '<?= base_url(); ?>api/gambar/add', true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	// API PUT
	function kirimKontak(data) {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-berhasil-kontak").style.display = "block";
				}
			}
		};

		xhr.open("PUT", "<?= base_url('api/kontak/set') ?>", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	function kirimVisiMisi(data) {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-berhasil").style.display = "block";
					
					resetMisi();
					deletePagination();
					getTotHalVM(true);
				}
			}
		};

		xhr.open("PUT", "<?= base_url('api/visimisi/set') ?>", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	function putDataGambar(nama = "") {
		let xhr = new XMLHttpRequest();
		let keterangan = document.getElementById("gk-edit").value;
		let data = {
			"id" : idToEdit,
			"url" : nama,
			"keterangan" : keterangan
		};
		
		xhr.onload = function () {
			if (xhr.status === 200) {
				deletePagination(true);
				getTotHalGambar();
				resetGambar();
				getGambar(halaman);
				
				document.getElementById("notif-edit-gambar").style.display = null;
			}
		};
		
		xhr.open('PUT', '<?= base_url(); ?>api/gambar/set', true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	// API DELETE
	function deleteVisiMisi() {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-del-berhasil").style.display = "block";
					
					resetMisi();
					deletePagination();
					getTotHalVM(true);
				}
			}
		};

		xhr.open("DELETE", "<?= base_url('api/visimisi/delete/') ?>" + idToDelete, true);
		xhr.send();
	}
	
	function deleteGambar() {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-del-gambar").style.display = "block";
					
					resetGambar();
					getGambar();
				}
			}
		};

		xhr.open("DELETE", "<?= base_url('api/gambar/delete/') ?>" + idToDelete, true);
		xhr.send();
	}
	
	function fillInfo(isEdit = false) {
		let inputFile;
		formData = new FormData();
		
		if(isEdit) {
			inputFile = document.getElementById("gambar-edit");
		} else {
			inputFile = document.getElementById("gambar");
		}
		
		if('files' in inputFile) {
			if(inputFile.files.lengths != 0) {
				for(
					let i = 0; i < inputFile.files.length; i++
				) {
					let file  = inputFile.files[i];
					
					if('name' in file && isImage(file.name)) {
						if(
							'size' in file
							&& isSizeOk(file.size)
						) {
							formData.append(
								'gambar',
								file,
								file.name
							);
						}
					}
				}
			}
		}
	}
	
	function simpanKontak() {
		let telepon = document.getElementById("no-telp").value;
		let alamat = document.getElementById("alamat").value;
		let email = document.getElementById("email").value;
		
		let data = {
			telepon: telepon,
			email: email,
			alamat: alamat
		};
		
		kirimKontak(data);
	}
	
	function simpanVisiMisi() {
		let visiMisi = document.getElementById("vm-container").value;
		
		let data = {
			id: idToEdit,
			konten: visiMisi
		};
		
		kirimVisiMisi(data);
	}
	
	function tambahMisi() {
		let misi = document.getElementById("input-misi-baru").value;
		document.getElementById("input-misi-baru").value = "";
		
		let data = {
			konten: misi
		};
		
		kirimMisi(data);
	}
	
	function setIdToDelete(id) {
		idToDelete = id;
	}
	
	function hideAlert(element) {
		element.parentNode.style.display = "none";
	}
	
	function fillVisiMisi(data) {
		let visiMisiContainer = document.getElementById("tab_1");
		let misiContainer = visiMisiContainer.childNodes[11].childNodes[3];
		document.getElementById("visi-container").innerHTML = data.visi;
		
		data.misi.forEach(function(misi) {
			let misiRow = document.createElement("tr");
			let nomorContainer = document.createElement("td");
			let jenisContainer = document.createElement("td");
			let kontenContainer = document.createElement("td");
			let opsiContainer = document.createElement("td");
			let editButton = document.createElement("button");
			let hapusButton = document.createElement("button");
			
			let nomorText = document.createTextNode(nomor);
			let misiText = document.createTextNode(misi.konten);
			let jenisText = document.createTextNode("Misi");
			let editText = document.createTextNode("Edit");
			let whitespace = document.createTextNode(" ");
			let hapusText = document.createTextNode("Hapus");
			
			misiRow.setAttribute("class","misi-row");
			editButton.setAttribute("type","button");
			editButton.setAttribute("class","btn btn-primary btn-xs");
			editButton.setAttribute("data-toggle","modal");
			editButton.setAttribute("data-target","#visimisi-edit");
			editButton.setAttribute("onclick","getVMData(" + misi.id + ")");
			hapusButton.setAttribute("type","button");
			hapusButton.setAttribute("class","btn btn-danger btn-xs");
			hapusButton.setAttribute("data-toggle","modal");
			hapusButton.setAttribute("data-target","#visimisi-hapus");
			hapusButton.setAttribute("onclick","setIdToDelete(" + misi.id + ")");
			
			nomorContainer.append(nomorText);
			jenisContainer.append(jenisText);
			kontenContainer.append(misiText);
			editButton.append(editText);
			hapusButton.append(hapusText);
			opsiContainer.append(editButton);
			opsiContainer.append(whitespace);
			opsiContainer.append(hapusButton);
			misiRow.append(nomorContainer);
			misiRow.append(jenisContainer);
			misiRow.append(kontenContainer);
			misiRow.append(opsiContainer);
			misiContainer.append(misiRow);
			
			nomor++;
		});
	}
	
	function fillGambar(data) {
		let gambarContainer = document.getElementById("tab_2");
		let gambarTBody = gambarContainer.childNodes[21].childNodes[3];
		
		if(data.length > 0) {
			document.getElementById("data-empty-gambar").style.display = "none";
			document.getElementById("data-table-gambar").style.display = null;
			
			data.forEach(function(gambar) {
				let gambarRow = document.createElement("tr");
				let nomorContainer = document.createElement("td");
				let namaContainer = document.createElement("td");
				let keteranganContainer = document.createElement("td");
				let imgContainer = document.createElement("td");
				let gambarImg = document.createElement("img");
				let opsiContainer = document.createElement("td");
				let editButton = document.createElement("button");
				let hapusButton = document.createElement("button");
				
				let nomorText = document.createTextNode(nomor);
				let namaText = document.createTextNode(gambar.url);
				let keteranganText = document.createTextNode(gambar.keterangan);
				let editText = document.createTextNode("Edit");
				let whitespace = document.createTextNode(" ");
				let hapusText = document.createTextNode("Hapus");
				
				gambarRow.setAttribute("class","gambar-row");
				gambarImg.setAttribute("src","<?= base_url('assets/images/'); ?>" + gambar.url);
				gambarImg.setAttribute("width","200px");
				gambarImg.setAttribute("height","auto");
				editButton.setAttribute("type","button");
				editButton.setAttribute("class","btn btn-primary btn-xs");
				editButton.setAttribute("data-toggle","modal");
				editButton.setAttribute("data-target","#edit-gambar");
				editButton.setAttribute("onclick","getGambarDatum(" + gambar.id + ")");
				hapusButton.setAttribute("type","button");
				hapusButton.setAttribute("class","btn btn-danger btn-xs");
				hapusButton.setAttribute("data-toggle","modal");
				hapusButton.setAttribute("data-target","#gambar-hapus");
				hapusButton.setAttribute("onclick","setIdToDelete(" + gambar.id + ")");
				
				nomorContainer.append(nomorText);
				namaContainer.append(namaText);
				keteranganContainer.append(keteranganText);
				imgContainer.append(gambarImg);
				editButton.append(editText);
				hapusButton.append(hapusText);
				opsiContainer.append(editButton);
				opsiContainer.append(whitespace);
				opsiContainer.append(hapusButton);
				gambarRow.append(nomorContainer);
				gambarRow.append(namaContainer);
				gambarRow.append(keteranganContainer);
				gambarRow.append(imgContainer);
				gambarRow.append(opsiContainer);
				gambarTBody.append(gambarRow);
				
				nomor++;
			});
		}
	}
	
	function fillPagination(jmlHalaman, isGambar = false) {
		if(jmlHalaman > 0) {
			let paginationParent;
			
			if(isGambar) {
				paginationParent = document.getElementById("pagination-gambar");
			} else {
				paginationParent = document.getElementById("pagination");
			}
			
			paginationParent.style.display = null;
			
			// Isi data
			for(let i=0; i<jmlHalaman+2; i++) {
				// Deklarasi elemen
				let paginationList = document.createElement("li");
				let paginationAnchor = document.createElement("a");
				let paginationText;
				
				if(i==0) {
					if(isGambar) {
						paginationList.setAttribute("onclick", "prev(true)");
					} else {
						paginationList.setAttribute("onclick", "prev()");
					}
					
					paginationText = document.createTextNode("Previous");
				} else if(i==jmlHalaman+1) {
					if(isGambar) {
						paginationList.setAttribute("onclick", "next(true)");
					} else {
						paginationList.setAttribute("onclick", "next()");
					}
					
					paginationText = document.createTextNode("Next");
				} else {
					if(isGambar) {
						paginationList.setAttribute("onclick", "openHalaman(" + i + ", true)");
					} else {
						paginationList.setAttribute("onclick", "openHalaman(" + i + ")");
					}
					
					paginationText = document.createTextNode(i);
				}
				
				// Isi konten elemen
				if(isGambar) {
					paginationList.setAttribute("class", "page-item link-pagination-gambar");
				} else {
					paginationList.setAttribute("class", "page-item link-pagination");
				}
				
				paginationAnchor.setAttribute("class", "page-link");
				paginationAnchor.setAttribute("href", "#");
				
				// Gabungkan elemen
				paginationAnchor.append(paginationText);
				paginationList.append(paginationAnchor);
				paginationParent.append(paginationList);
			}
		}
	}
	
	function fillKontak(data) {
		document.getElementById("no-telp").value = data.telepon;
		document.getElementById("alamat").value = data.alamat;
		document.getElementById("email").value = data.email;
	}
	
	function fillVMData(data) {
		document.getElementById("vm-container").value = data.konten;
		idToEdit = data.id;
	}
	
	function fillGambarEdit(data) {
		document.getElementById("gk-edit").value = data.keterangan;
		idToEdit = data.id;
	}
	
	function setHalAktif(halaman = 1, isGambar = false) {
		let paginationList;
		
		if(isGambar) {
			paginationList = document.getElementsByClassName("link-pagination-gambar");
		} else {
			paginationList = document.getElementsByClassName("link-pagination");
		}
		
		if(isGambar) {
			resetPagination(true);
		} else {
			resetPagination();
		}
		
		paginationList[halaman].classList.add("active");
		
		if(isGambar) {
			paginationList[halaman].id = "pagination-aktif-gambar";
		} else {
			paginationList[halaman].id = "pagination-aktif";
		}
	}
	
	function resetPagination(isGambar = false) {
		let activePagination;
		
		if(isGambar) {
			activePagination = document.getElementById("pagination-aktif-gambar");
		} else {
			activePagination = document.getElementById("pagination-aktif");
		}
		
		if(activePagination != null) {
			activePagination.id = "";
			activePagination.classList.remove("active");
		}
	}
	
	function deletePagination(isGambar = false) {
		let activePagination;
		
		if(isGambar) {
			activePagination = document.getElementById("pagination-gambar");
		} else {
			activePagination = document.getElementById("pagination");
		}
		
		while(activePagination.childNodes.length) {
			activePagination.childNodes[0].parentNode.removeChild(activePagination.childNodes[0]);
		}
	}
	
	function prev(isGambar = false) {
		if(halaman>1) {
			if(isGambar) {
				resetGambar();
				getGambar(--halaman);
			} else {
				resetMisi();
				getVisiMisi(--halaman);
			}
		}
	}
	
	function next(isGambar = false) {
		if(halaman<totalHalaman) {
			if(isGambar) {
				resetGambar();
				getGambar(++halaman);
			} else {
				resetMisi();
				getVisiMisi(++halaman);
			}
		}
	}
	
	function openHalaman(halamanTujuan, isGambar = false) {
		halaman = halamanTujuan;
		
		if(isGambar) {
			resetGambar();
			getGambar(halaman);
		} else {
			resetMisi();
			getVisiMisi(halaman);
		}
	}
	
	function resetMisi() {
		let dataMisi = document.getElementsByClassName("misi-row");
		
		while(dataMisi.length) {
			dataMisi[0].parentNode.removeChild(dataMisi[0]);
		}
	}
	
	function resetGambar() {
		let dataGambar = document.getElementsByClassName("gambar-row");
		
		while(dataGambar.length) {
			dataGambar[0].parentNode.removeChild(dataGambar[0]);
		}
	}
	
	function isImage(name) {
		let toReturn = false;
		let extension = name.split(".").pop().toLowerCase();
		
		if(
			extension == 'jpg' || extension == 'png'
			|| extension == 'jpeg'
		) {
			toReturn = true;
		}
		
		return toReturn;
	}
	
	function isSizeOk(size) {
		let toReturn = false;
		
		if(size <= 2000000) {
			toReturn = true;
		}
		
		return toReturn;
	}
	
	getTotHalVM();
	getTotHalGambar();
	getVisiMisi();
	getGambar();
	getKontak();
</script>

<?php
$this->load->view('template/footer');
?>
