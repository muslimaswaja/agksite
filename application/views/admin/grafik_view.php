<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->

<style type="text/css">
	.active {
		background-color: #F39C12;
		color: #fff;
	}

	.example-modal .modal {
		position: relative;
		top: auto;
		bottom: auto;
		right: auto;
		left: auto;
		display: block;
		z-index: 1;
	}

	.example-modal .modal {
		background: transparent !important;
	}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content" style="background-color: #ecf0f5">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<span id="judul-kuisioner"></span>

			<div class="pull-right">
				<a href="#">
					<button type="button" class="btn btn-primary export-btn">Export ke Excel</button>
				</a>
			</div>
		</h1>
		<br>

	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<!-- isi grafik -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="https://cdn.jsdelivr.net/npm/alasql@0.4"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.12/xlsx.core.min.js"></script>
<script>
	//~ Deklarasi variabel
	let idKuisioner = "<?= $id; ?>";




	// Jquery untuk chart
	$(function () {
		var responData = getRespon();
		var kuisioner = getKuisioner();

		for (let i = 0; i < kuisioner.soal.length; i++) {
			let jenisJawaban = kuisioner.soal[i].id_jenis_jawaban;
			makeContainer(kuisioner, i);
			if (jenisJawaban == 1 || jenisJawaban == 3) {
				makeChart(kuisioner, responData, i);
			}
			else {
				makeTable(kuisioner, responData, i);
			}
		}

		$(document).on('click', '.export-btn', function (e) {
			// e.preventDefault();
			exportExcel(kuisioner, responData);
		});
	});

	function makeTable(kuisioner, responData, index) {
		let idSoal = kuisioner.soal[index].id;
		let tableCanvas = "#chartCanvas" + index;

		let responsiveTable = $("<div></div>").addClass("table-responsive");
		$(tableCanvas).before(responsiveTable)

		let table = $("<table></table>").addClass("table table-hover");
		$(".table-responsive:last").append(table);

		let tbody = $("<tbody></tbody>");
		$("table:last").append(tbody);

		let responToQuestion = getResponToQuestion(kuisioner, responData, idSoal);

		for (let i = 0; i < responToQuestion.length; i++) {
			if (i == 5) {
				break;
			}
			let tr = $("<tr></tr>");
			$(".table-responsive:last tbody").append(tr);

			let td = $("<td></td>").text(responToQuestion[i]);
			$(".table-responsive:last tr:last").append(td);
		}
		if (responToQuestion.length > 5) {
			let note = $("<p></p>").text("Untuk melihat data yang lebih lengkap, silahkan menekan tombol export:");
			let exportBtn = $("<button></button>").addClass("btn btn-primary export-btn").text("Export ke Excel");
			$(tableCanvas).before(note, exportBtn);
		}
	}

	function makeChart(kuisioner, responData, index) {
		let chartType;
		let legend;
		let idSoal = kuisioner.soal[index].id;
		let scale = {};
		let chartCanvas = document.getElementById("chartCanvas" + index).getContext('2d');

		if (kuisioner.soal[index].id_jenis_jawaban == 3) {
			chartType = "bar";
			legend = false;
			scale =  {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			};
		}
		else if (kuisioner.soal[index].id_jenis_jawaban == 1) {
			chartType = "pie";
			legend = true;
		}

		// respon untuk pertanyaan yang terkait
		let responToQuestion = getResponToQuestion(kuisioner, responData, idSoal);

		// hitung jumlah respon
		let listJawaban = getPilihan(kuisioner, index);
		let hasilJawaban = hitungJawaban(listJawaban, responToQuestion);
		
		let chartOptions = {
			//Boolean - Whether we should show a stroke on each segment
			segmentShowStroke: true,
			//String - The colour of each segment stroke
			segmentStrokeColor: '#fff',
			//Number - The width of each segment stroke
			segmentStrokeWidth: 2,
			//Number - The percentage of the chart that we cut out of the middle
			percentageInnerCutout: 50, // This is 0 for Pie charts
			//Number - Amount of animation steps
			animationSteps: 100,
			//String - Animation easing effect
			animationEasing: 'easeOutBounce',
			//Boolean - Whether we animate the rotation of the Doughnut
			animateRotate: true,
			//Boolean - Whether we animate scaling the Doughnut from the centre
			animateScale: false,
			//Boolean - whether to make the chart responsive to window resizing
			responsive: true,
			// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			maintainAspectRatio: true,
			legend: {
				display: legend,
			},
			scales:scale,
		}

		let count = [];
		let jawaban = [];
		for (let jawab in hasilJawaban) {
			count.push(hasilJawaban[jawab]);
			jawaban.push(jawab);
		}

		let bgcolor = generateBgColor(responToQuestion.length);

		let chartData = [{
			data: count,
			backgroundColor: bgcolor,
		}];
		let chartLabels = jawaban;
		let chart = new Chart(chartCanvas, {
			type: chartType,
			data: {
				datasets: chartData,

				// These labels appear in the legend and in the tooltips when hovering different arcs
				labels: chartLabels,
			},
			options: chartOptions,
		});
	}

	function makeContainer(kuisioner, index) {
		// buat class div column
		let divCol = $("<div></div>").addClass("col-md-6");
		$(".content .row").append(divCol);

		// buat chart area
		let areaSelector = ".content .row .col-md-6:last";

		divCol = $("<div></div>").addClass("box box-warning");
		$(areaSelector).append(divCol);

		divCol = $("<div></div>").addClass("box-header with-border");
		let divCol2 = $("<div></div>").addClass("box-body");
		$(areaSelector + " .box").append(divCol, divCol2);

		let h3Judul = $("<h3></h3>").addClass("box-title judul-soal").text(kuisioner.soal[index].soal);
		divCol = $("<div></div>").addClass("box-tools pull-right");
		$(areaSelector + " .box-header").append(h3Judul, divCol);

		// let button1 = $("<button></button>").attr({
		// 	"type": "button",
		// 	"class": "btn btn-box-tool",
		// 	"data-toogle": "collapse",
		// 	"data-target": ".box-body",
		// }).html('<i class="fa fa-minus"></i>');

		// $(areaSelector + " .box-tools").append(button1);

		// canvas
		let canvas = $("<canvas></canvas>").attr({
			"id": "chartCanvas" + index,
			"style": "height: 230px",
		})
		$(areaSelector + " .box-body").append(canvas);
	}

	function generateBgColor(n) {
		let colors = [];
		for (let i = 0; i < n; i++) {
			colors[i] = "rgba("
			for (let j = 0; j < 3; j++) {
				colors[i] += Math.floor((Math.random() * 255) + 0) + ",";
			}
			colors[i] = colors[i].slice(0, -1);
			colors[i] += ")";
		}
		return colors;
	}

	function hitungJawaban(listJawaban, responToQuestion) {
		for (let i = 0; i < responToQuestion.length; i++) {
			if (responToQuestion[i] in listJawaban) {
				listJawaban[responToQuestion[i]] += 1;
			}
		}
		return listJawaban;
	}

	function getPilihan(kuisioner, index) {
		let listJawaban = {};
		let jawabanObj = kuisioner.soal[index].jawaban;
		for (let i = 0; i < jawabanObj.length; i++) {
			listJawaban[jawabanObj[i].jawaban] = 0;
		}

		return listJawaban;
	}

	function getResponToQuestion(kuisioner, responData, idSoal) {
		let responToQuestion = [];
		for (let i = 0; i < responData.length; i++) {
			for (let j = 0; j < responData[i].respon.length; j++) {
				if (responData[i].respon[j].id_soal == idSoal) {
					responToQuestion.push(responData[i].respon[j].respon);
				}
			}
		}
		// console.log(responToQuestion);
		return responToQuestion;
	}
	//~ API GET
	function getKuisioner() {
		var url = "<?= base_url('api/kuisioner/get/') ?>" + idKuisioner;
		var kuisioner;
		$.ajax({
			url: url,
			dataType: 'json',
			async: false,
			success: function (data) {
				kuisioner = data;
				fillKuisioner(kuisioner);
			}
		});
		return kuisioner;
	}

	function getRespon() {
		var url = "<?= base_url('api/respon/get/') ?>" + idKuisioner;
		var responData;
		$.ajax({
			url: url,
			dataType: 'json',
			async: false,
			success: function (data) {
				responData = data;
			}
		});
		return responData;
	}

	//~ Content filler
	function fillKuisioner(kuisioner) {
		// console.log(data);

		document.getElementById("judul-kuisioner").innerHTML = kuisioner.judul;
	}
	window.exportExcel = function exportExcel(kuisioner, responData) {
		let index = 1;
		let judulSoal = kuisioner.judul;
		let dataObj = generateData(kuisioner, responData);
		let testObj = [{ a: 100, b: 200 }];
		var opts = [{ sheetid: 'Form Kuisioner', header: true }];
		var res = alasql('SELECT INTO XLSX("' + judulSoal + '.xlsx",?) FROM ?', [opts, [dataObj]]);
	}

	function generateData(kuisioner, responData) {
		let dataObj = [];
		for (let i = 0; i < responData.length; i++) {
			dataObj[i] = {};
			dataObj[i]['timestamp'] = responData[i].waktu_input;

			for (let j = 0; j < kuisioner.soal.length; j++) {
				let soalObj = kuisioner.soal[j];
				let soalId = soalObj.id;
				dataObj[i][soalObj.soal] = "";

				// jika cekbox, jawaban digabung.
				if (soalObj.jenis_jawaban.id == "3") {
					for (l = 0; l < responData[i].respon.length; l++) {
						if (responData[i].respon[l].id_soal == soalId) {
							dataObj[i][soalObj.soal] += responData[i].respon[l].respon + ', ';
						}
					}
					dataObj[i][soalObj.soal] = dataObj[i][soalObj.soal].trim();
					dataObj[i][soalObj.soal] = dataObj[i][soalObj.soal].slice(0, -1);
				}
				else {
					for (let l = 0; l < responData[i].respon.length; l++) {
						if (responData[i].respon[l].id_soal == soalId) {
							dataObj[i][soalObj.soal] += responData[i].respon[l].respon;
						}
					}
				}
			}
		}
		return dataObj;
	}

	function getJudulSoal() {
		let judulSoal = new Array();
		$(".judul-soal").text(function (index, content) {
			judulSoal[index] = content;
		});
		return judulSoal;
	}

</script>

<?php
$this->load->view('template/footer');
?>