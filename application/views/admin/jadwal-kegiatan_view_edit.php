<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content" style="background-color: #ecf0f5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Jadwal Kegiatan
        <small>it all starts here</small>
      </h1> 
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-9">
                    <input id="nama-acara" type="text" class="form-control" placeholder="Masukkan Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-9">
                    <input id="tanggal" type="date" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Waktu</label>
                  <div class="col-sm-9">
                    <input id="waktu" type="time" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-9">
                    <textarea id="keterangan-acara" class="form-control" rows="3" placeholder="Masukkan Keterangan"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Tempat</label>
                  <div class="col-sm-9">
                    <input id="cari-tempat" type="text" class="form-control" placeholder="Masukkan Alamat">
                    <div id="map" style="height:300px"></div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-default">Cancel</button>
                <button type="button" class="btn btn-primary pull-right" onclick="simpanJadwal()">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
	var map;
	var markers = [];
	var koordinat;
	var idJadwal;
	
	function getJadwal() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				fillJadwal(JSON.parse(this.responseText));
			}
		};

		xhttp.open("GET", "<?= base_url('api/jadwal/get/'.$id) ?>", true);
		xhttp.send();
	}
	
	function fillJadwal(data) {		
		let tanggalWaktu = data.tgl_waktu.split(" ");
		idJadwal = data.id;
		
		document.getElementById("nama-acara").value = data.nama;
		document.getElementById("keterangan-acara").value = data.keterangan;
		document.getElementById("cari-tempat").value = data.alamat;
		document.getElementById("tanggal").value = tanggalWaktu[0];
		document.getElementById("waktu").value = tanggalWaktu[1];
		
		markThis(data.koordinat,"api");
	}
	
	function simpanJadwal() {
		let namaAcara = document.getElementById("nama-acara").value;
		let keteranganAcara = document.getElementById("keterangan-acara").value;
		let tempatAcara = document.getElementById("cari-tempat").value;
		let tanggal = document.getElementById("tanggal").value;
		let waktu = document.getElementById("waktu").value;
		let tanggalWaktu = tanggal + " " + waktu;
		
		let data = {
			id: idJadwal,
			nama: namaAcara,
			tgl_waktu: tanggalWaktu,
			alamat: tempatAcara,
			koordinat: koordinat,
			keterangan: keteranganAcara
		};
		
		kirimData(data);
	}

	function initMap(lat=-7.282687, lng=112.787542) {
		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: lat, lng: lng},
			zoom: 15
		});
		
		/** Place Search */
		var searchInput = /** @type {!HTMLInputElement} */(document.getElementById('cari-tempat'));
		var autocomplete = new google.maps.places.Autocomplete(searchInput);
		var infowindow = new google.maps.InfoWindow();
		var marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29)
		});
		
		autocomplete.bindTo('bounds', map);
		
		autocomplete.addListener('place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			
			var place = autocomplete.getPlace();
			
			if (!place.geometry) {
				window.alert("Lokasi yang berhubungan dengan '" + place.name + "' tidak ditemukan");
				return;
			}

			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(17); 
			}
			
			markThis(place.geometry.location, "latLng");
		});
		/** End Place Search */
		
		map.addListener('click', function(event) {
			markThis(event);
		});
	}
	
	function markThis(sumber, jenis="event") {
		setMapOnAll(null);
		var latToMark;
		var lngToMark;
		
		if(jenis == "event") {
			latToMark = sumber.latLng.lat();
			lngToMark = sumber.latLng.lng();
		} else if(jenis == "api") {
			var latLngToMark = [];
			latLngToMark = sumber.split(" ");
			latToMark = parseFloat(latLngToMark[0]);
			lngToMark = parseFloat(latLngToMark[1]);
		} else {
			var latLngToMark = [];
			sumber = sumber.toString().replace("(", "").replace(")", "");
			latLngToMark = sumber.split(", ");
			latToMark = parseFloat(latLngToMark[0]);
			lngToMark = parseFloat(latLngToMark[1]);
		}
		
		koordinat = latToMark + " " + lngToMark;
		
		var marker = new google.maps.Marker({
			position: {
				lat: latToMark,
				lng: lngToMark
			},
			map: map
		});
		
		map.setZoom(17);
		
		markers.push(marker);
	}
	
	function setMapOnAll(map) {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
		}
	}
	
	function kirimData(data) {
		let xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText) > 0) {
					<?php $this->session->set_flashdata('status', 'true'); ?>
					
					window.open("<?= base_url('admin/jadwal'); ?>","_self");
				}
			}
		};

		xhr.open("PUT", "<?= base_url('api/jadwal/set') ?>", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
	}
	
	getJadwal();
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALRsqgjFhR4kVeLXBqQYD6pZVrg7pb-so&libraries=places&callback=initMap"></script>

<?php
$this->load->view('template/footer');
?>
