<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content" style="background-color: #ecf0f5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Jadwal Kegiatan
        <small>a.k.a. Jadwal kegiatan karang taruna Gebang Kidul</small>
      </h1>
      <br>
      
    </section>

    <!-- Main content -->
    <section class="content">

		<!-- Konfirmasi data ditambahkan -->
		<?php
		if($this->session->flashdata('status') != null) { ?>
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="icon fa fa-check"></i> Data berhasil disimpan.
			</div>
		<?php } ?>
		
		<!-- Konfirmasi data dihapuss -->
		<div id="notif-del-berhasil" class="alert alert-danger alert-dismissible" style="display:none">
			<button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
			<i class="icon fa fa-check"></i> Data berhasil dihapus.
		</div>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List</h3>

          <div class="box-tools pull-right">
            <a href="<?= base_url('admin/jadwal/tambah'); ?>"><button type="button" class="btn btn-box-tool btn-default btn-s">Tambah</button></a>
          </div>
        </div>
        <div class="box-body">
			<table id="data-table" class="table table-hover" style="display:none">
				<tbody id="jadwal-container">
					<tr>
						<th style="width:5%">No</th>
						<th>Nama</th>
						<th>Tanggal</th>
						<th>Waktu</th>
						<th>Tempat</th>
						<th>Keterangan</th>
						<th style="width:10%">Aksi</th>
					</tr>
				</tbody>
			</table>

			<!-- Keterangan kalo isi tabel kosong -->
			<div id="data-empty" class="callout callout-warning">
                <h4>Data Kosong</h4>
                <p>Klik <a href="<?= base_url('admin/jadwal/tambah'); ?>">sini</a> untuk menambahkan data</p>
            </div>
            <!-- Akhir keterangan -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
          <ul class="pagination" id="pagination" style="display:none"></ul>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

	<!-- Modal Hapus-->
	<div class="modal fade" id="modal-hapus">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					
					<h4 class="modal-title">Hapus Data ?</h4>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteJadwal()">Hapus</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
	let halaman = 1;
	let nomor = 1;
	let totalHalaman;
	var idToDelete;
	
	function getDataKegiatan(halaman = 1) {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				nomor = ((halaman - 1) * 10) + 1;
				
				fillJadwalKegiatan(JSON.parse(this.responseText));
				setHalAktif(halaman);
			}
		};

		xhttp.open("GET", "<?= base_url('api/jadwal/get_all/') ?>" + halaman, true);
		xhttp.send();
	}
	
	function getTotHalJadwal(isDelete = false) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				totalHalaman = JSON.parse(this.responseText);
				
				if(isDelete) {
					if(totalHalaman == 0) {
						ifDataEmpty();
					} else if(halaman > totalHalaman) {
						prev();
					} else {
						openHalaman(halaman);
					}
				}
				
				fillPagination(totalHalaman);
			}
		};

		xhr.open("GET", "<?= base_url('api/jadwal/get_total_halaman') ?>", true);
		xhr.send();
	}
	
	function deleteJadwal() {
		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-del-berhasil").style.display = "block";
					
					resetDataKegiatan();
					deletePagination();
					getTotHalJadwal(true);
				}
			}
		};

		xhttp.open("DELETE", "<?= base_url('api/jadwal/delete/') ?>" + idToDelete, true);
		xhttp.send();
	}
	
	function fillJadwalKegiatan(data) {
		
		let jadwalParent = document.getElementById("jadwal-container");
		let i = 1;
		
		if(data.length > 0) {
			document.getElementById("data-empty").style.display = "none";
			document.getElementById("data-table").style.display = null;
			
			data.forEach(function(jadwal) {
				// Buat elemen untuk jadwal kegiatan
				let jadwalContainer = document.createElement("tr");
				let nomorContainer = document.createElement("td");
				let acaraContainer = document.createElement("td");
				let tanggalContainer = document.createElement("td");
				let waktuContainer = document.createElement("td");
				let lokasiContainer = document.createElement("td");
				let keteranganContainer = document.createElement("td");
				let controlContainer = document.createElement("td");
				let anchorEdit = document.createElement("a");
				let buttonEdit = document.createElement("button");
				let buttonHapus = document.createElement("button");

				// Set atribut elemen jadwal kegiatan
				jadwalContainer.id = "jadwal" + jadwal.id;
				jadwalContainer.className = "jadwal";
				anchorEdit.setAttribute("href", "<?= base_url("admin/jadwal/edit/"); ?>" + jadwal.id);
				buttonEdit.className = "btn btn-primary btn-xs";
				buttonEdit.setAttribute("type", "button");
				buttonHapus.setAttribute("type", "button");
				buttonHapus.setAttribute("data-toggle", "modal");
				buttonHapus.setAttribute("data-target", "#modal-hapus");
				buttonHapus.setAttribute("onclick", "setIdToDelete(" + jadwal.id + ")");
				buttonHapus.className = "btn btn-danger btn-xs";

				// Buat konten untuk jadwal kegiatan
				let tgl_waktu = jadwal.tgl_waktu.split(" ");
				let tanggal = document.createTextNode(tgl_waktu[0].replace(/-/g,"/"));
				let jam = document.createTextNode(tgl_waktu[1].substr(0, 5));
				let lokasi = document.createTextNode(jadwal.alamat);
				let acara = document.createTextNode(jadwal.nama);
				let keterangan = document.createTextNode(jadwal.keterangan);
				let edit = document.createTextNode("Edit");
				let whitespace = document.createTextNode(" ");
				let hapus = document.createTextNode("Hapus");
				let nomor = document.createTextNode(i);

				// Menambahkan jadwal ke container jadwal
				nomorContainer.appendChild(nomor);
				acaraContainer.appendChild(acara);
				waktuContainer.appendChild(jam);
				tanggalContainer.appendChild(tanggal);
				lokasiContainer.appendChild(lokasi);
				keteranganContainer.appendChild(keterangan);
				buttonEdit.appendChild(edit);
				anchorEdit.appendChild(buttonEdit);
				buttonHapus.appendChild(hapus);
				controlContainer.appendChild(anchorEdit);
				controlContainer.appendChild(whitespace);
				controlContainer.appendChild(buttonHapus);
				jadwalContainer.appendChild(nomorContainer);
				jadwalContainer.appendChild(acaraContainer);
				jadwalContainer.appendChild(tanggalContainer);
				jadwalContainer.appendChild(waktuContainer);
				jadwalContainer.appendChild(lokasiContainer);
				jadwalContainer.appendChild(keteranganContainer);
				jadwalContainer.appendChild(controlContainer);
				jadwalParent.appendChild(jadwalContainer);
				
				i++;
			});
		}
	}
	
	function fillPagination(jmlHalaman) {
		if(jmlHalaman > 0) {
			let paginationParent = document.getElementById("pagination");
			
			// Isi data
			for(let i=0; i<jmlHalaman+2; i++) {
				// Deklarasi elemen
				let paginationList = document.createElement("li");
				let paginationAnchor = document.createElement("a");
				let paginationText;
				
				if(i==0) {
					paginationList.setAttribute("onclick", "prev()");
					paginationText = document.createTextNode("Previous");
				} else if(i==jmlHalaman+1) {
					paginationList.setAttribute("onclick", "next()");
					paginationText = document.createTextNode("Next");
				} else {
					paginationList.setAttribute("onclick", "openHalaman(" + i + ")");
					paginationText = document.createTextNode(i);
				}
				
				// Isi konten elemen
				paginationList.setAttribute("class", "page-item link-pagination");
				paginationAnchor.setAttribute("class", "page-link");
				paginationAnchor.setAttribute("href", "#");
				
				// Gabungkan elemen
				paginationAnchor.append(paginationText);
				paginationList.append(paginationAnchor);
				paginationParent.append(paginationList);
			}
		}
	}
	
	function setIdToDelete(id) {
		idToDelete = id;
	}
	
	function hideAlert(element) {
		element.parentNode.style.display = "none";
	}
	
	function resetDataKegiatan() {
		let dataKegiatan = document.getElementsByClassName("jadwal");
		
		while(dataKegiatan.length) {
			dataKegiatan[0].parentNode.removeChild(dataKegiatan[0]);
		}
	}
	
	function prev() {
		if(halaman>1) {
			resetDataKegiatan();
			getDataKegiatan(--halaman);
		}
	}
	
	function next() {
		if(halaman<totalHalaman) {
			resetDataKegiatan();
			getDataKegiatan(++halaman);
		}
	}
	
	function openHalaman(halamanTujuan) {
		halaman = halamanTujuan;
		
		resetDataKegiatan();
		getDataKegiatan(halaman);
	}
	
	function setHalAktif(halaman = 1) {
		let paginationList = document.getElementsByClassName("link-pagination");
		let paginationContainer = document.getElementById("pagination");
		
		if(totalHalaman > 0) {
			paginationContainer.style.display = null;
			
			resetPagination();
			paginationList[halaman].classList.add("active");
			paginationList[halaman].id = "pagination-aktif";
		}
	}
	
	function resetPagination() {
		let activePagination = document.getElementById("pagination-aktif");
		
		if(activePagination != null) {
			activePagination.id = "";
			activePagination.classList.remove("active");
		}
	}
	
	function deletePagination() {
		let activePagination = document.getElementById("pagination");
		
		while(activePagination.childNodes.length) {
			activePagination.childNodes[0].parentNode.removeChild(activePagination.childNodes[0]);
		}
	}
	
	function ifDataEmpty() {
		document.getElementById("data-empty").style.display = null;
		document.getElementById("data-table").style.display = "none";
		document.getElementById("pagination").style.display = "none";
	}
	
	getTotHalJadwal();
	getDataKegiatan();
</script>

<?php
$this->load->view('template/footer');
?>
