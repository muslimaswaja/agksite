<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content" style="background-color: #ecf0f5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Anggota
        <small>Daftar Anggota Karang Taruna Gebang Kidul</small>
      </h1>
      <br>
      
    </section>

    <!-- Main content -->
    <section class="content">
		
		<!-- Konfirmasi data dihapuss -->
		<div id="notif-del-berhasil" class="alert alert-danger alert-dismissible" style="display:none">
			<button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
			<i class="icon fa fa-check"></i> Data berhasil dihapus.
		</div>

		<?php if($this->session->flashdata('status') != null) { ?>
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="icon fa fa-check"></i> Data berhasil disimpan.
			</div>
		<?php } ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List</h3>

          <div class="box-tools pull-right">
			  <a href="<?= base_url('admin/anggota/tambah'); ?>">
				  <button type="button" class="btn btn-box-tool btn-default btn-s" data-toggle="tooltip">Tambah</button>
			  </a>
          </div>
        </div>
        <div class="box-body">
			<!-- Keterangan kalo isi tabel kosong -->
			<div id="data-empty" class="callout callout-warning">
				<h4>Data Kosong</h4>
				<p>Klik <a href="<?= base_url('admin/anggota/tambah'); ?>">sini</a> untuk menambahkan data</p>
			</div>
			<!-- Akhir keterangan -->
          <table id="data-table" class="table table-hover" style="display:none">
			  <thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Jabatan</th>
					<th>Alamat</th>
					<th>No Telp</th>
					<th>Foto</th>
					<th>Aksi</th>
				</tr>
			  </thead>
			  
			  <tbody id="list-anggota-container"></tbody>
              </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
            <ul class="pagination" id="pagination"></ul>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Modal Hapus-->
	<div class="modal fade" id="modal-hapus">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					
					<h4 class="modal-title">Hapus Data ?</h4>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteDatumAnggota()">Hapus</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
	// Deklarasi variabel
	let halaman = 1;
	let nomor = 1;
	let totalHalaman;
	let idToDelete;
  
	// API GET
	function getDataAnggota(halaman = 1) {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				nomor = ((halaman - 1) * 10) + 1;
				
				fillDataAnggota(JSON.parse(this.responseText));
				setHalAktif(halaman);
			}
		};

		xhttp.open("GET", "<?= base_url('api/anggota/get_all/') ?>" + halaman, true);
		xhttp.send();
	}
	
	function getTotHalAnggota(isDelete = false) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				totalHalaman = JSON.parse(this.responseText);

				if(isDelete) {
					if(halaman > totalHalaman) {
						prev();
					} else {
						openHalaman(halaman);
					}
				}

				fillPagination(totalHalaman);
			}
		};

		xhr.open("GET", "<?= base_url('api/anggota/get_total_halaman') ?>", true);
		xhr.send();
	}
	
	// API DELETE
	function deleteDatumAnggota() {
		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					document.getElementById("notif-del-berhasil").style.display = null;
					
					resetListAnggota();
					deletePagination();
					getTotHalAnggota(true);
				}
			}
		};

		xhttp.open("DELETE", "<?= base_url('api/anggota/delete/') ?>" + idToDelete, true);
		xhttp.send();
	}
	
	// Content Filler
	function fillDataAnggota(data) {
		let listAnggotaContainer = document.getElementById("list-anggota-container");
		
		if(data.length > 0) {
			document.getElementById("data-empty").style.display = "none";
			document.getElementById("data-table").style.display = null;
			
			data.forEach(function(anggota) {
				// Buat elemen untuk jadwal kegiatan
				let anggotaContainer = document.createElement("tr");
				let nomorContainer = document.createElement("td");
				let namaContainer = document.createElement("td");
				let jabatanContainer = document.createElement("td");
				let alamatContainer = document.createElement("td");
				let noTelpContainer = document.createElement("td");
				let fotoContainer = document.createElement("td");
				let controlContainer = document.createElement("td");
				let anchorEdit = document.createElement("a");
				let buttonEdit = document.createElement("button");
				let buttonHapus = document.createElement("button");

				// Set atribut elemen jadwal kegiatan
				anggotaContainer.id = "anggota" + anggota.id;
				anggotaContainer.className = "anggota";
				anchorEdit.setAttribute("href", '<?= base_url("admin/anggota/edit/"); ?>' + anggota.id);
				buttonEdit.className = "btn btn-primary btn-xs";
				buttonEdit.setAttribute("type", "button");
				buttonHapus.setAttribute("type", "button");
				buttonHapus.setAttribute("data-toggle", "modal");
				buttonHapus.setAttribute("data-target", "#modal-hapus");
				buttonHapus.setAttribute("onclick", "setIdToDelete(" + anggota.id + ")");
				buttonHapus.className = "btn btn-danger btn-xs";

				// Buat konten untuk jadwal kegiatan
				let nomorText = document.createTextNode(nomor);
				let nama = document.createTextNode(anggota.nama);
				let jabatan = document.createTextNode(anggota.jabatan.jabatan);
				let alamat = document.createTextNode(anggota.alamat);
				let noTelp = document.createTextNode(anggota.no_telp);
				let foto = document.createTextNode(anggota.url_foto);
				let edit = document.createTextNode("Edit");
				let whitespace = document.createTextNode(" ");
				let hapus = document.createTextNode("Hapus");

				// Menambahkan jadwal ke container jadwal
				nomorContainer.appendChild(nomorText);
				namaContainer.appendChild(nama);
				jabatanContainer.appendChild(jabatan);
				alamatContainer.appendChild(alamat);
				noTelpContainer.appendChild(noTelp);
				fotoContainer.appendChild(foto);
				buttonEdit.appendChild(edit);
				anchorEdit.appendChild(buttonEdit);
				buttonHapus.appendChild(hapus);
				controlContainer.appendChild(anchorEdit);
				controlContainer.appendChild(whitespace);
				controlContainer.appendChild(buttonHapus);
				anggotaContainer.appendChild(nomorContainer);
				anggotaContainer.appendChild(namaContainer);
				anggotaContainer.appendChild(jabatanContainer);
				anggotaContainer.appendChild(alamatContainer);
				anggotaContainer.appendChild(noTelpContainer);
				anggotaContainer.appendChild(fotoContainer);
				anggotaContainer.appendChild(controlContainer);
				listAnggotaContainer.appendChild(anggotaContainer);
				
				nomor++;
			});
		}
	}
	
	function fillPagination(jmlHalaman) {
		if(jmlHalaman > 0) {
			let paginationParent = document.getElementById("pagination");

			// Isi data
			for(let i=0; i<jmlHalaman+2; i++) {
				// Deklarasi elemen
				let paginationList = document.createElement("li");
				let paginationAnchor = document.createElement("a");
				let paginationText;

				if(i==0) {
					paginationList.setAttribute("onclick", "prev()");
					paginationText = document.createTextNode("Previous");
				} else if(i==jmlHalaman+1) {
					paginationList.setAttribute("onclick", "next()");
					paginationText = document.createTextNode("Next");
				} else {
					paginationList.setAttribute("onclick", "openHalaman(" + i + ")");
					paginationText = document.createTextNode(i);
				}

				// Isi konten elemen
				paginationList.setAttribute("class", "page-item link-pagination");
				paginationAnchor.setAttribute("class", "page-link");
				paginationAnchor.setAttribute("href", "#");

				// Gabungkan elemen
				paginationAnchor.append(paginationText);
				paginationList.append(paginationAnchor);
				paginationParent.append(paginationList);
			}
		}
	}

	// Other Functions
	function resetListAnggota() {
		let dataKegiatan = document.getElementsByClassName("anggota");
		
		while(dataKegiatan.length) {
			dataKegiatan[0].parentNode.removeChild(dataKegiatan[0]);
		}
	}
	
	function setIdToDelete(id) {
		idToDelete = id;
	}
	
	function deletePagination() {
		let activePagination = document.getElementById("pagination");
		
		while(activePagination.childNodes.length) {
			activePagination.childNodes[0].parentNode.removeChild(activePagination.childNodes[0]);
		}
	}
	
	function hideAlert(element) {
		element.parentNode.style.display = "none";
	}
	
  function prev() {
    if(halaman>1) {
      resetListAnggota();
      getDataAnggota(--halaman);
    }
  }
  
  function next() {
    if(halaman<totalHalaman) {
      resetListAnggota();
      getDataAnggota(++halaman);
    }
  }
  
  function openHalaman(halamanTujuan) {
    halaman = halamanTujuan;
    
    resetListAnggota();
    getDataAnggota(halaman);
  }

  function setHalAktif(halaman = 1) {
    let paginationList = document.getElementsByClassName("link-pagination");
    
    resetPagination();
    paginationList[halaman].classList.add("active");
    paginationList[halaman].id = "pagination-aktif";
  }
  
  function resetPagination() {
    let activePagination = document.getElementById("pagination-aktif");
    
    if(activePagination != null) {
      activePagination.id = "";
      activePagination.classList.remove("active");
    }
  }

  getTotHalAnggota();
  getDataAnggota();
</script>

<?php
$this->load->view('template/footer');
?>
