<?php
$this->load->view('template/navbar');
?>

<!-- =============================================== -->
  
  <style type="text/css">
    .active{
      background-color: #F39C12;
      color: #fff;
    }

    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content" style="background-color: #ecf0f5">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kuisioner
        <small>Daftar Kuisioner Karang Taruna Gebang Kidul</small>
      </h1>
      <br>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="notif-berhasil-kuisioner" class="alert alert-success alert-dismissible" style="display:none">
          <button type="button" class="close" onclick="hideAlert(this)" aria-hidden="true">&times;</button>
          <i class="icon fa fa-check"></i> Data berhasil ditambahkan.
      </div>

      <!-- Default box -->
      <div id="data-table" class="box" style="display:none">
        <div class="box-header with-border">
          <h3 class="box-title">Judul Kuisioner</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool btn-default btn-s" data-toggle="modal" data-target="#modal-default">Tambah Kuisioner</button>
          </div>
        </div>
        <div class="box-body" style="padding: 20px">
          <table class="table table-hover">
			<tbody id="kuisioner-container"></tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-cente">
           <center><ul class="pagination" id="pagination"></ul></center>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

      <!-- Keterangan kalo isi tabel kosong -->
		<div id="data-empty" class="callout callout-warning">
			<h4>Data Kosong</h4>
			<p>Klik <a href="#" data-toggle="modal" data-target="#modal-default">sini</a> untuk menambahkan data</p>
		</div>
		<!-- Akhir keterangan -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="control-sidebar-bg"></div>

  <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Kuisioner</h4>
              </div>
              <div class="modal-body">
                <form role="form" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Judul</label>
                  <div class="col-sm-9">
                    <input id="judul-kuisioner-baru" type="text" class="form-control" placeholder="Masukkan Judul">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <p class="btn btn-default" data-dismiss="modal">Cancel</p>
                <p class="btn btn-primary pull-right" onclick="buatKuisioner()">Submit</p>
              </div>
            </form>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal Hapus-->
  		<div class="modal fade" id="modal-hapus">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hapus Data ?</h4>
              </div>
              <!-- <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div> -->
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteKuisioner()">Hapus</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
</div>
<!-- ./wrapper -->

<script>
	//~ Deklarasi variabel
	let halaman = 1;
	let nomor = 1;
	let totalHalaman;
	var idToDelete;
	
	//~ API GET
	function getKuisioner() {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				nomor = ((halaman - 1) * 10) + 1;
				
				fillKuisionerList(JSON.parse(this.responseText));
				setHalAktif(halaman);
			}
		};

		xhttp.open("GET", "<?= base_url('api/kuisioner/get_all/') ?>" + halaman, true);
		xhttp.send();
	}

	function getTotHalKuis(isDelete = false) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				totalHalaman = JSON.parse(this.responseText);
				
				if(isDelete) {
					if(halaman > totalHalaman) {
						prev();
					} else {
						openHalaman(halaman);
					}
				}
				
				fillPagination(totalHalaman);
			}
		};

		xhr.open("GET", "<?= base_url('api/kuisioner/get_total_halaman') ?>", true);
		xhr.send();
	}
	
	function setAktif(id) {
		let xhttp = new XMLHttpRequest();
		let data = {
			"id" : id.replace("kuisioner","")
		};

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					setKuisionerAktif(id);
				}
			}
		};

		xhttp.open("PUT", "<?= base_url('api/kuisioner/set_active') ?>", true);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(JSON.stringify(data));
	}
	
	function setNonaktif(id) {
		let xhttp = new XMLHttpRequest();
		let data = {
			"id" : id.replace("kuisioner","")
		};

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					resetKuisionerAktif();
				}
			}
		};

		xhttp.open("PUT", "<?= base_url('api/kuisioner/set_nonactive') ?>", true);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(JSON.stringify(data));
	}
	
	function setKuisionerAktif(id) {
		resetKuisionerAktif();
		
		let tombolAktif = document.getElementById(id).childNodes[2]
			.childNodes[0].childNodes[0];
		let tombolNonaktif = document.getElementById(id).childNodes[2]
			.childNodes[0].childNodes[1];
		
		tombolAktif.className = "btn btn-secondary btn-xs active";
		tombolNonaktif.className = "btn btn-secondary btn-xs";
	}
	
	function setIdToDelete(id) {
		idToDelete = id;
	}
	
	function deleteKuisioner() {
		let xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(JSON.parse(this.responseText)) {
					resetKuisioner();
					getKuisioner();
				}
			}
		};

		xhttp.open("DELETE", "<?= base_url('api/kuisioner/delete/') ?>" + idToDelete, true);
		xhttp.send();
	}
	
	function resetKuisionerAktif() {
		let tombolAktif = document.getElementsByClassName("option1");
		let tombolNonaktif = document.getElementsByClassName("option2");
		
		for(let i=0; i<tombolAktif.length;i++) {
			tombolAktif[i].parentNode.className = "btn btn-secondary btn-xs";
		}
		
		for(let i=0; i<tombolNonaktif.length;i++) {
			tombolNonaktif[i].parentNode.className = "btn btn-secondary active btn-xs";
		}
	}
	
	function resetKuisioner() {
		let kuisionerParent = document.getElementById("kuisioner-container");
		
		while(kuisionerParent.firstChild) {
			kuisionerParent.removeChild(kuisionerParent.firstChild);
		}
	}
	
	function fillKuisionerList(data) {
		let kuisionerParent = document.getElementById("kuisioner-container");
		
		if(data.length > 0) {
			document.getElementById("data-empty").style.display = "none";
			document.getElementById("data-table").style.display = "block";
			
			data.forEach(function(kuisioner) {
				// Buat elemen untuk kuisioner
				let kuisionerContainer = document.createElement("tr");
				let nomorContainer = document.createElement("td");
				let judulContainer = document.createElement("td");
				let controlContainer = document.createElement("td");
				let switchContainer = document.createElement("div");
				let editButton = document.createElement("button");
				let editAnchor = document.createElement("a");
				let hasilButton = document.createElement("button");
				let hasilAnchor = document.createElement("a");
				let hapusButton = document.createElement("button");
				let onContainer = document.createElement("label");
				let offContainer = document.createElement("label");
				let onButton = document.createElement("input");
				let offButton = document.createElement("input");

				// Set atribut elemen kuisioner
				kuisionerContainer.id = "kuisioner" + kuisioner.id;
				switchContainer.className = "btn-group btn-group-toggle btn-default";
				
				if(kuisioner.aktif == 1) {
					onContainer.className = "btn btn-secondary active btn-xs";
					offContainer.className = "btn btn-secondary btn-xs";
				} else {
					onContainer.className = "btn btn-secondary btn-xs";
					offContainer.className = "btn btn-secondary active btn-xs";
				}
				
				onButton.className = "option1";
				onContainer.setAttribute("onclick","setAktif(this.parentNode.parentNode.parentNode.id)");
				offContainer.setAttribute("onclick","setNonaktif(this.parentNode.parentNode.parentNode.id)");
				offButton.className = "option2";
				onButton.setAttribute("type", "radio");
				onButton.setAttribute("name", "options");
				offButton.setAttribute("type", "radio");
				offButton.setAttribute("name", "options");
				offButton.setAttribute("checked", "true");
				switchContainer.setAttribute("data-toggle", "buttons");
				editButton.setAttribute("type", "button");
				editAnchor.setAttribute("href", "<?= base_url('admin/kuisioner/edit/'); ?>" + kuisioner.id);
				hasilButton.setAttribute("type", "button");
				hasilAnchor.setAttribute("href", "<?= base_url('admin/grafik/'); ?>" + kuisioner.id)
				hapusButton.setAttribute("type", "button");
				hapusButton.setAttribute("data-toggle", "modal");
				hapusButton.setAttribute("data-target", "#modal-hapus");
				hapusButton.setAttribute("onclick", "setIdToDelete(" + kuisioner.id + ")");
				editButton.className = "btn btn-primary btn-xs";
				hasilButton.className = "btn btn-success btn-xs";
				hapusButton.className = "btn btn-danger btn-xs";

				// Buat konten untuk kuisioner
				let nomorText = document.createTextNode(nomor);
				let judul = document.createTextNode(kuisioner.judul);
				let on = document.createTextNode("Aktif");
				let off = document.createTextNode("Nonaktif");
				let whitespace1 = document.createTextNode(" ");
				let edit = document.createTextNode("Edit");
				let whitespace2 = document.createTextNode(" ");
				let hasil = document.createTextNode("Grafik");
				let whitespace3 = document.createTextNode(" ");
				let hapus = document.createTextNode("Hapus");

				// Menambahkan kuuisi ke container kuisioner
				nomorContainer.appendChild(nomorText);
				judulContainer.appendChild(judul);
				onContainer.appendChild(onButton);
				onContainer.appendChild(on);
				offContainer.appendChild(offButton);
				offContainer.appendChild(off);
				switchContainer.appendChild(onContainer);
				switchContainer.appendChild(offContainer);
				editButton.appendChild(edit);
				editAnchor.appendChild(editButton);
				hasilButton.appendChild(hasil);
				hasilAnchor.appendChild(hasilButton);
				hapusButton.appendChild(hapus);
				controlContainer.appendChild(switchContainer);
				controlContainer.appendChild(whitespace1);
				controlContainer.appendChild(editAnchor);
				controlContainer.appendChild(whitespace2);
				controlContainer.appendChild(hasilAnchor);
				controlContainer.appendChild(whitespace3);
				controlContainer.appendChild(hapusButton);
				kuisionerContainer.appendChild(nomorContainer);
				kuisionerContainer.appendChild(judulContainer);
				kuisionerContainer.appendChild(controlContainer);
				kuisionerParent.appendChild(kuisionerContainer);
				
				nomor++;
			});
		}
	}

	function fillPagination(jmlHalaman) {
		if(jmlHalaman > 0) {
			let paginationParent = document.getElementById("pagination");
			
			// Isi data
			for(let i=0; i<jmlHalaman+2; i++) {
				// Deklarasi elemen
				let paginationList = document.createElement("li");
				let paginationAnchor = document.createElement("a");
				let paginationText;
				
				if(i==0) {
					paginationList.setAttribute("onclick", "prev()");
					paginationText = document.createTextNode("Previous");
				} else if(i==jmlHalaman+1) {
					paginationList.setAttribute("onclick", "next()");
					paginationText = document.createTextNode("Next");
				} else {
					paginationList.setAttribute("onclick", "openHalaman(" + i + ")");
					paginationText = document.createTextNode(i);
				}
				
				// Isi konten elemen
				paginationList.setAttribute("class", "page-item link-pagination");
				paginationAnchor.setAttribute("class", "page-link");
				paginationAnchor.setAttribute("href", "#");
				
				// Gabungkan elemen
				paginationAnchor.append(paginationText);
				paginationList.append(paginationAnchor);
				paginationParent.append(paginationList);
			}
		}
	}
	
	function buatKuisioner() {
		let judul = document.getElementById("judul-kuisioner-baru").value;
		
		let xhttp = new XMLHttpRequest();
		let data = {
			"judul" : judul
		};

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				let id = JSON.parse(this.responseText);
				
				if(id > 0) {
					window.open("<?= base_url('/admin/kuisioner/edit/'); ?>" + id,"_self");
				}
			}
		};

		xhttp.open("POST", "<?= base_url('api/kuisioner/add') ?>", true);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(JSON.stringify(data));
	}
	
	function hideAlert(element) {
		element.parentNode.style.display = "none";
	}

	function prev() {
		if(halaman>1) {
			resetKuisioner();
			getKuisioner(--halaman);
		}
	}
	
	function next() {
		if(halaman<totalHalaman) {
			resetKuisioner();
			getKuisioner(++halaman);
		}
	}
	
	function openHalaman(halamanTujuan) {
		halaman = halamanTujuan;
		
		resetKuisioner();
		getKuisioner(halaman);
	}

	function setHalAktif(halaman = 1) {
		let paginationList = document.getElementsByClassName("link-pagination");
		
		resetPagination();
		paginationList[halaman].classList.add("active");
		paginationList[halaman].id = "pagination-aktif";
	}
	
	function resetPagination() {
		let activePagination = document.getElementById("pagination-aktif");
		
		if(activePagination != null) {
			activePagination.id = "";
			activePagination.classList.remove("active");
		}
	}
	
	getTotHalKuis();
	getKuisioner();
</script>

<?php
$this->load->view('template/footer');
?>
