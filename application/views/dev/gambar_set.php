<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
	<body>
		<input type="file" id="gambar" name="gambar" onchange="fillInfo()"> <br>
		<input type="text" id="id" name="id"> <br>
		<input type="text" id="keterangan" name="keterangan">
		<button onclick="upload()">Upload</button>
		<script>
			var formData = new FormData();
			var isImageFilled = false;
			
			function fillInfo() {
				var inputFile = document.getElementById("gambar");
				
				if('files' in inputFile) {
					if(inputFile.files.lengths != 0) {
						for(
							let i = 0; i < inputFile.files.length; i++
						) {
							let file  = inputFile.files[i];
							
							if('name' in file && isImage(file.name)) {
								if(
									'size' in file
									&& isSizeOk(file.size)
								) {
									formData.append(
										'gambar',
										file,
										file.name
									);
								}
							}
						}
					}
					
					isImageFilled = true;
				}
			}
			
			function upload() {
				if(isImageFilled) {
					let xhr = new XMLHttpRequest();
				
					xhr.onload = function () {
					  if (xhr.status === 200) {
						if(this.responseText) {
							let data = JSON.parse(this.responseText);
					
							if(data.status) {
								saveData(data.url);
							}
						}
					  }
					};
					
					xhr.open('POST', '<?= base_url(); ?>api/gambar/upload', true);
					xhr.send(formData);
				} else {
					saveData();
				}
			}
			
			function saveData(nama="") {
				let xhr = new XMLHttpRequest();
				let keterangan = document.getElementById("keterangan").value;
				let id = document.getElementById("id").value;
				let data = {
					"id" : id,
					"url" : nama,
					"keterangan" : keterangan
				};
				
				xhr.onload = function () {
					if (xhr.status === 200) {
						console.log(this.responseText);
					}
				};
				
				xhr.open('POST', '<?= base_url(); ?>api/gambar/set', true);
				xhr.setRequestHeader("Content-type", "application/json");
				xhr.send(JSON.stringify(data));
			}
			
			function isImage(name) {
				let toReturn = false;
				let extension = name.split(".").pop().toLowerCase();
				
				if(
					extension == 'jpg' || extension == 'png'
					|| extension == 'jpeg'
				) {
					toReturn = true;
				}
				
				return toReturn;
			}
			
			function isSizeOk(size) {
				let toReturn = false;
				
				if(size <= 2000000) {
					toReturn = true;
				}
				
				return toReturn;
			}
		</script>
	</body>
</html>
