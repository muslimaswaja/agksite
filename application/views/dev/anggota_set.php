<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
	<body>
		<input type="file" id="foto" name="foto" onchange="fillInfo()"> <br>
		<input type="text" id="id" name="id"> <br>
		<input type="text" id="nama" name="nama" placeholder="nama"> <br>
		<input type="text" id="jabatan_id" name="jabatan_id" placeholder="jabatan_id"> <br>
		<input type="text" id="alamat" name="alamat" placeholder="alamat"> <br>
		<input type="text" id="no_telp" name="no_telp" placeholder="no_telp"> <br>
		<button onclick="upload()">Upload</button>
		<script>
			var formData = new FormData();
			var isImageFilled = false;
			
			function fillInfo() {
				var inputFile = document.getElementById("foto");
				
				if('files' in inputFile) {
					if(inputFile.files.lengths != 0) {
						for(
							let i = 0; i < inputFile.files.length; i++
						) {
							let file  = inputFile.files[i];
							
							if('name' in file && isImage(file.name)) {
								if(
									'size' in file
									&& isSizeOk(file.size)
								) {
									formData.append(
										'foto',
										file,
										file.name
									);
									
									isImageFilled = true;
								}
							}
						}
					}
				}
			}
			
			function upload() {
				if(isImageFilled) {
					let xhr = new XMLHttpRequest();
				
					xhr.onload = function () {
					  if (xhr.status === 200) {
						if(this.responseText) {
							let data = JSON.parse(this.responseText);
					
							if(data.status) {
								saveData(data.url);
							}
						}
					  }
					};
					
					xhr.open('POST', '<?= base_url(); ?>api/anggota/upload', true);
					xhr.send(formData);
				} else {
					saveData();
				}
			}
			
			function saveData(url="") {
				let xhr = new XMLHttpRequest();
				let id = document.getElementById("id").value;
				let nama = document.getElementById("nama").value;
				let jabatan_id = document
					.getElementById("jabatan_id").value;
				let alamat = document
					.getElementById("alamat").value;
				let no_telp = document
					.getElementById("no_telp").value;
				let data = {
					"id" : id,
					"url_foto" : url,
					"nama" : nama,
					"jabatan_id" : jabatan_id,
					"alamat" : alamat,
					"no_telp" : no_telp,
				};
				console.log(data);
				
				xhr.onload = function () {
					if (xhr.status === 200) {
						console.log(this.responseText);
					} else {
						console.log("gagal");
					}
				};
				
				xhr.open(
					'POST',
					'<?= base_url(); ?>api/anggota/set',
					true
				);
				xhr.setRequestHeader(
					"Content-type",
					"application/json"
				);
				xhr.send(JSON.stringify(data));
			}
			
			function isImage(name) {
				let toReturn = false;
				let extension = name.split(".").pop().toLowerCase();
				
				if(
					extension == 'jpg' || extension == 'png'
					|| extension == 'jpeg'
				) {
					toReturn = true;
				}
				
				return toReturn;
			}
			
			function isSizeOk(size) {
				let toReturn = false;
				
				if(size <= 2000000) {
					toReturn = true;
				}
				
				return toReturn;
			}
		</script>
	</body>
</html>
