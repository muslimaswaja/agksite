$(function () {
    // Hapus pertanyaan
    $(document).on('click', '.btn-hapus', function (e) {
        e.preventDefault();
        var position = $(this).parentsUntil(".content", ".cont-pertanyaan").index() - 1;
        $(".cont-pertanyaan:eq(" + position + ")").remove();
        $(".no-pertanyaan:eq(" + position + ")").text($(".no-pertanyaan:eq(" + position + ")").text() - 1);
        $(".no-pertanyaan:gt(" + position + ")").text($(".no-pertanyaan:gt(" + position + ")").text() - 1);
    });

    //Tambah pertanyaan
    $(".btn-tambah-pertanyaan").click(function () {
        buat_pertanyaan();
        var parent_index = $(".box-content:last").parentsUntil(".main-content", ".cont-pertanyaan").index() - 1;
        display_pilihan(parent_index, "radio");
    });

    // tambah opsi
    $(document).on("click", ".btn-opsi", function (e) {
        e.preventDefault();
        var parent_index = get_question_box_index(this);
        var tipe = $(".cont-pertanyaan:eq(" + parent_index + ") .TanyaJawab .tipe-soal").val();
        display_pilihan(parent_index, tipe);
    });

    //hapus opsi
    $(document).on("click", ".btn-del-opsi", function(e){
        e.preventDefault();
        var opsi_index = $(this).parentsUntil(".TanyaJawab", ".pilihan-jawaban").index() - 2;
        hapus_opsi(opsi_index, get_question_box_index(this));
    });

    //ganti tipe pilihan
    $(document).on("change", ".tipe-soal", function (e) {
        e.preventDefault();
        var tipe = $(this).val();
        ganti_tipe(get_question_box_index(this), tipe);
    });
});

function display_judul(){
    var input = $("<input>").attr({
        "type": "text",
        "id": "judul-kuisioner",
        "class": "form-control BorderKuisioner pertanyaan",
        "placeholder": "Judul Formulir",
    });
    $(".content-header small").before(input);
}

function buat_pertanyaan() {
    var next_position = $(".cont-pertanyaan:last").index() + 1;
    
    if(next_position == 0){
		next_position = 1;
	}
	
    var box_body = $("<div></div>").attr({
        "class": "box-body cont-pertanyaan",
        "id": "box-" + (next_position),
    });

    $(".tambah-pertanyaan").before(box_body);

    var curr_position = $(".cont-pertanyaan:last").index();

    var box = $("<div></div").addClass("box box-warning box-content-border box-content");
    $(".cont-pertanyaan:eq(" + (curr_position - 1) + ")").append(box);

    var box_header = $("<div></div>").addClass("box-header");
    // div untuk pertanyaan dan jawaban
    var tanya_jawab = $("<div></div>").addClass("box-body TanyaJawab");
    $(".cont-pertanyaan:last .box-content").append(box_header, tanya_jawab);

    var box_tools = $("<div></div>").attr({
        "class": "box-tools pull-right",
    });
    var no_soal_area = $("<div></div>").addClass("text-left");
    $(".box-content:last .box-header").append(box_tools, no_soal_area);

    // Tambah button untuk hapus soal
    var btn_hapus = $("<button>").text("Hapus").css("color", "#fff").attr({
        "class": "btn btn-box-tool btn-danger btn-s btn-hapus"
    });
    $(".box-content:last .box-header .box-tools").append(btn_hapus);

    // Tambah no. soal
    var no_soal = $("<span></span>").addClass("no-pertanyaan").text(curr_position);
    $(".box-content:last .box-header .text-left").append(no_soal, ".");

    // div untuk tambah input pertanyaan
    var input_pertanyaan = $("<div></div>").addClass("form-group div-pertanyaan");
    // div untuk menampilkan jenis pilihan
    var jenis_pilihan = $("<div></div>").addClass("form-group text-right div-jenis");
    $(".box-content:last .TanyaJawab").append(input_pertanyaan, jenis_pilihan);

    //Input untuk pertanyaan
    var input_text = $("<input>").attr({
        "type": "text",
        "class": "form-control BorderKuisioner pertanyaan",
        "name": "pertanyaan",
        "value": "",
        "placeholder": "Masukkan pertanyaan",
        "autocomplete": "off",
    });
    $(".box-content:last .div-pertanyaan").append(input_text);

    //jenis pilihan
    var input_select = $("<select></select>").attr({
        "class": "tipe-soal form-control-sm",
        "name": "TipeSoal",
        "autocomplete": "off",
    });
    $(".box-content:last .div-jenis").append(input_select);
    var option = new Array();
    option[0] = $("<option></option>").val("radio").text("Pilihan Ganda");
    option[1] = $("<option></option>").val("text").text("Jawaban Singkat");
    option[2] = $("<option></option>").val("checkbox").text("Kotak Centang");
    $(".box-content:last .tipe-soal").append(option);
}

function display_pilihan(parent_index, tipe) {
    var selector = ".cont-pertanyaan:eq(" + parent_index + ") .box-content ";
    //div untuk menampilkan pilihan
    var pilihan = $("<div></div>").addClass("form-inline pilihan-jawaban");
    $(selector + ".TanyaJawab").append(pilihan);

    // Daftar pilihan
    selector = selector + ".pilihan-jawaban:last ";
    var form_group = $("<div></div>").addClass("form-group");
    var input_group = $("<div></div>").addClass("input-group");
    $(selector).append(form_group, input_group);

    var label_type = $("<div></div>").addClass(tipe);
    $(selector + ".form-group").append(label_type);
    var input_type = $("<input>").attr({
        "type": tipe,
        "name": "jawaban",
        "disabled": "",
    });
    var label = $("<label></label>").addClass(tipe + "-inline").append(input_type);
    $(selector + "." + tipe).append(label);

    input_text = $("<input>").attr({
        "type": "text",
        "class": "form-control BorderKuisioner",
        "name": "input-pilihan",
        "value": "",
        "placeholder": "Masukkan pilihan",
        "autocomplete": "off",
    });
    var div_btn_tambah = $("<div></div>").addClass("input-group-btn");
    $(selector + ".input-group").append(input_text, div_btn_tambah);

    var btn_tambah_pil = $("<button></button>").addClass("btn btn-warning btn-opsi").html("<i class=\"glyphicon glyphicon-plus-sign\"></i>");
    var btn_hapus_pil = $("<button></button>").addClass("btn btn-danger btn-del-opsi").html("<i class=\"glyphicon glyphicon-minus-sign\"></i>");
    if($(selector).index() == 2){
        btn_hapus_pil.attr("disabled","");
    }
    $(selector + ".input-group-btn").append(btn_tambah_pil, btn_hapus_pil);
}

function hapus_opsi(opsi_index, parent_index){
    var selector = ".cont-pertanyaan:eq(" + parent_index + ") .pilihan-jawaban:eq(" + opsi_index + ")";
    $(selector).remove();
}

function get_question_box_index(button) {
    return $(button).parentsUntil(".main-content", ".cont-pertanyaan").index() - 1;
}

function ganti_tipe(parent_index, tipe) {
    var selector = ".cont-pertanyaan:eq(" + parent_index + ") .pilihan-jawaban ";
    if (tipe == "text") {
        $(selector + ".form-group:first").hide();
        $(selector + ".form-group:gt(0)").remove();
        $(selector + ".input-group:gt(0)").remove();
        $(selector + ".input-group-btn").hide();
        $(selector + ".input-group input").val("");
        $(selector + ".input-group input").attr("placeholder", "Jawaban Singkat");        
    }
    else {
        if($(selector + ".form-group").css("display") == "none"){
            $(selector + ".form-group").show();
            $(selector + ".input-group-btn").show();
            $(selector + ".input-group input").attr("placeholder", "Masukan pilihan");                
        }
        var tipe_old = $(selector + ".form-group div:first").attr("class");
        $(selector + ".form-group div").removeClass(tipe_old).addClass(tipe);
        $(selector + ".form-group label").removeClass(tipe_old + "-inline").addClass(tipe + "-inline");
        $(selector + ".form-group div label input").attr("type", tipe);
    }
}
